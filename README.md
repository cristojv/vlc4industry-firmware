# Project: Visible Light Communications (VLC) for Industry 4.0
## Block: Firmware

### Description:
🖥️ Firmware developed for Arduino boards.

It includes the following firmware:
1) *vlc* folder contains the firmware for the **download (tx)** link 💡.
   The tx uses the serial port 🔌 to receive instructions to be sent to
   the robot 🤖.
2) *occ* folder contains the firmware for the **upload (rx)** link 📷.
   The rx uses RGB LEDs to communicate with a camera.
3) *robot* folder contains the firmware for controlling the **robot**.
   It coordinates the vlc rx, the robot and the occ tx.