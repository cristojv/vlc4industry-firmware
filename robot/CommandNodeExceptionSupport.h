/**
 * The class CommandNode implements the nodes of a tree where the data allocated in each
 * node is a pointer to a function.
 */

#ifndef CommandNode_h
#define CommandNode_h

#define BUFFER_LENGTH_NODE 15
#define MULTIPLIER 2
#define COMMAND_LENGTH 5

#include "Arduino.h"

typedef const char *(*executable)(const char *arguments);

class CommandNodeExceptionSupport {
private:

	CommandNodeExceptionSupport **_subCommandNodes;
	char **_subCommandNames;
	unsigned int _subCommandsNumber;
	unsigned int _bufferLength;

public:
	unsigned int getBufferLength();
	CommandNodeExceptionSupport();
	CommandNodeExceptionSupport(unsigned int bufferLength);
	CommandNodeExceptionSupport(executable execute);
	void addSubCommand(CommandNodeExceptionSupport *subCommandNode, const char *subCommandName);
	CommandNodeExceptionSupport *getSubCommand(const char *subCommandName);
	CommandNodeExceptionSupport *getSubCommand(int index);
	const char *getSubCommandName(int index);
	unsigned int getSubCommandsNumber();
	executable execute;

};

#endif /* CommandNode_h */
