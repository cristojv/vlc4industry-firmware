/**
 * This class implements a tree made of commandNodes, each commandNode can have another
 * commandNode inside, or a pointer to a function. The tree has methods to navigate through
 * it, and obtain the data in the leaf nodes.
 */

#include "Arduino.h"
#include "CommandTreeExceptionSupport.h"
#include "CommandNodeExceptionSupport.h"

/**
 * This constructor instanciates a tree with a root node created using the default constructor.
 */
CommandTreeExceptionSupport::CommandTreeExceptionSupport() {

	CommandNodeExceptionSupport *root = new CommandNodeExceptionSupport();

	_rootNode = root;
	_actualNode = _rootNode;

}

/**
 * By using this constructor, the tree is initialized with a root node that can contain a number
 * of nodes defined in the parameter.
 */
CommandTreeExceptionSupport::CommandTreeExceptionSupport(int subCommandsNumber) {

	CommandNodeExceptionSupport *root = new CommandNodeExceptionSupport(subCommandsNumber);

	_rootNode = root;
	_actualNode = _rootNode;

}

/**
 * This method is used for debugging, it instanciates a tree with a root node that contains a
 * pointer to a function, so the root node is a leaf node.
 */
CommandTreeExceptionSupport::CommandTreeExceptionSupport(executable execute) {

	CommandNodeExceptionSupport *root = new CommandNodeExceptionSupport(execute);

	_rootNode = root;
	_actualNode = _rootNode;

}

void CommandTreeExceptionSupport::init(int subCommandsNumber) {

	CommandNodeExceptionSupport *root = new CommandNodeExceptionSupport(subCommandsNumber);

	_rootNode = root;
	_actualNode = _rootNode;

}

/**
 * This method makes the attribute _actualNode point the root node of the tree.
 */
void CommandTreeExceptionSupport::toRootNode() {

	_actualNode = _rootNode;

}

/**
 * This method adds a new node to the node pointed by _actualNode by using the default constructor.
 */
void CommandTreeExceptionSupport::addSubCommand(const char *subCommandName) {

	CommandNodeExceptionSupport *cmd = new CommandNodeExceptionSupport();

	_actualNode->addSubCommand(cmd, subCommandName);

}

/**
 * This method adds a node to the node pointed by _actualNode, this new node can hold up to
 * subCommandsNumber nodes, specified by the parameter, if no subCommandsNumber is specified, it
 * uses the default constructor instead.
 */
void CommandTreeExceptionSupport::addSubCommand(const char *subCommandName, int subCommandsNumber = 0) {

	CommandNodeExceptionSupport *cmd = new CommandNodeExceptionSupport(subCommandsNumber);

	_actualNode->addSubCommand(cmd, subCommandName);

}

/**
 * This method adds a leaf node to the actual node, by specifying the function pointer as an
 * argument.
 */
void CommandTreeExceptionSupport::addSubCommand(const char *subCommandName, executable execute) {

	CommandNodeExceptionSupport *cmd = new CommandNodeExceptionSupport(execute);

	_actualNode->addSubCommand(cmd, subCommandName);

}

/**
 * This method changes the node that _actualNode points to, by making it point the node retrieved
 * from the nodes contained in the actual node at the time of calling this method.
 */
void CommandTreeExceptionSupport::accessSubCommand(const char *subCommandName) {

	_actualNode = _actualNode->getSubCommand(subCommandName);

}

/**
 *
 */
int CommandTreeExceptionSupport::existsSubCommand(const char *subCommandName) {

	for (unsigned int i = 0; i < _actualNode->getSubCommandsNumber(); i++) {
		if (strcmp(_actualNode->getSubCommandName(i), subCommandName) == 0) {
			return 1;
		}
	}
	return 0;

}

/**
 * This method executes the function pointer contained in the actual node, with the parameters
 * specified.
 */
const char *CommandTreeExceptionSupport::execute(const char *arguments) {
 if (_actualNode->getBufferLength() == 0){
	return _actualNode->execute(arguments);
 }
 	return NULL;

}

bool CommandTreeExceptionSupport::isFunctionNode() {
	if (_actualNode->getBufferLength() == 0) {
		return true;
	} else {
		return false;
	}
}

/**
 * This is a debugging method, to know if the commandTree was created without problem, or to test
 * if a subnode was accessed correctly.
 */
const char *CommandTreeExceptionSupport::test() {

	return "Hola";

}
