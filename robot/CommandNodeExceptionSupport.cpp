/**
 * The class CommandNode implements the nodes of a tree where the data allocated in each
 * node is a pointer to a function.
 */

#include <stdlib.h>
#include <string.h>

#include "Arduino.h"
#include "CommandNodeExceptionSupport.h"

/**
 * This constructor instanciates a commandNode intermediate node, with a default node buffer of 5
 * subcommands, if possible, this constructor should be avoided by specifying the number of
 * subcommands in the parameters of the constructor.
 */
CommandNodeExceptionSupport::CommandNodeExceptionSupport() {

	execute = NULL;
	_subCommandsNumber = 0;

	_subCommandNodes = malloc(sizeof(CommandNodeExceptionSupport *)*BUFFER_LENGTH_NODE);
	_subCommandNames = malloc(sizeof(char *)*BUFFER_LENGTH_NODE);
	_bufferLength = BUFFER_LENGTH_NODE;

}

/**
 * This constructor assigns the new commandNode a buffer specified in the parameters.
 */
CommandNodeExceptionSupport::CommandNodeExceptionSupport(unsigned int bufferLength = 0) {

	/* CommandNodes that do not have a function pointer are intermediate nodes or
	   the root node, so it it necessary to asssign a buffer for possible
	   subcommand nodes. */

	execute = NULL;
	_subCommandsNumber = 0;

	/* If a number of subcommands has been passed by parameter, it will assign that
	   number of buffer positions for possible subcommands, if not, a default number
	   of subcommands will be set (5). */

	if (bufferLength != 0) {
		_subCommandNodes = malloc(sizeof(CommandNodeExceptionSupport **)*bufferLength);
		_subCommandNames = malloc(sizeof(char *)*bufferLength);
		_bufferLength = bufferLength;
	} else {
		_subCommandNodes = malloc(sizeof(CommandNodeExceptionSupport **)*BUFFER_LENGTH_NODE);
		_subCommandNames = malloc(sizeof(char *)*BUFFER_LENGTH_NODE);
		_bufferLength = BUFFER_LENGTH_NODE;
	}

}

/**
 * This constructor instanciates a leaf node with a function pointer specified in the parameters,
 * the buffer of this new node for containing subnodes is 0 (leaf nodes don't have subnodes).
 */
CommandNodeExceptionSupport::CommandNodeExceptionSupport(executable execute) {

	this->execute = execute;
	_subCommandsNumber = 0;

	/* CommandNodes that have a function pointer are leaf nodes, they cannot have
	   subcommands, so _subCommandNodes is NULL pointer, and associated buffer will
	   be 0. */

	_subCommandNodes = NULL;
	_subCommandNames = NULL;
	_bufferLength = 0;

}

/**
 * This method adds a subcommand with the specified name if there is enough space, if there is not
 * enough space, the method reallocates memory for the subcommand and after that introduces it in
 * the tree.
 */
void CommandNodeExceptionSupport::addSubCommand(CommandNodeExceptionSupport *subCommandNode, const char *subCommandName) {

	if (_subCommandsNumber < _bufferLength) {
		_subCommandNodes[_subCommandsNumber] = subCommandNode;
		_subCommandNames[_subCommandsNumber] = subCommandName;

		_subCommandsNumber++;
	}

}

/**
 * This method returns the subcommand with the specified name if it exists, in other case it will
 * return a NULL pointer.
 */
CommandNodeExceptionSupport *CommandNodeExceptionSupport::getSubCommand(const char *subCommandName) {

	for (int i = 0; i < _subCommandsNumber; i++) {
		if (strcmp(_subCommandNames[i], subCommandName) == 0) {
			return _subCommandNodes[i];
		}
	}
	return NULL;

}

/**
 * This method returns the subcommand at a specified position if it exists, and if a valid index
 * is given, if these conditions are not met, it returns a NULL pointer.
 */
CommandNodeExceptionSupport *CommandNodeExceptionSupport::getSubCommand(int index) {

	if ((index < _subCommandsNumber) && (index >= 0)) {
		return _subCommandNodes[index];
	}
	return NULL;

}

const char *CommandNodeExceptionSupport::getSubCommandName(int index) {

	if ((index < _subCommandsNumber) && (index >= 0)) {
		return _subCommandNames[index];
	}
	return NULL;

}

unsigned int CommandNodeExceptionSupport::getSubCommandsNumber() {

	return this->_subCommandsNumber;

}

unsigned int CommandNodeExceptionSupport::getBufferLength(){
	return this->_bufferLength;
}
