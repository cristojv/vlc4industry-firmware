/*
	Encoder.cpp
*/
#include "Encoder.h"
/*
	The encode function encodes the data stored in the rawBuffer array and stores it into the encodedBuffer
	array, returning the resulting size of the encodedBuffer
*/
int Encoder::encodeData(uint8_t *rawBuffer, int rawSize, uint8_t *encodedBuffer, int encodedSize) {

	if (encodedSize < 2*rawSize) {
		return 0;
	}

	int encodedBufferPos = 0;

	for (int i = 0; i < rawSize; i++) {
		if (rawBuffer[i] <= COM_MAGIC) {
			encodedBuffer[encodedBufferPos] = COM_MAGIC;
			encodedBuffer[encodedBufferPos + 1] = COM_MAGIC + rawBuffer[i];
			encodedBufferPos += 2;
		} else {
			encodedBuffer[encodedBufferPos] = rawBuffer[i];
			encodedBufferPos++;
		}
	}

 int nonemptySize = encodedBufferPos;

  while (encodedBufferPos < encodedSize) {
    encodedBuffer[encodedBufferPos++] = 0;
  }

	return nonemptySize;

}

/*
	The decode function decodes the data stored in the encodedBuffer array and stores it into the decodedBuffer
	array, returning the resulting size of the decodedBuffer
*/
int Encoder::decodeData(uint8_t *encodedBuffer, int encodedSize, uint8_t *decodedBuffer, int decodedSize) {

	if (decodedSize < encodedSize) {
		return 0;
	}

	int decodedBufferPos = 0;

	for (int i = 0; i < encodedSize; i++) {
		if (encodedBuffer[i] == COM_MAGIC) {
			decodedBuffer[decodedBufferPos] = encodedBuffer[i + 1] - COM_MAGIC;
			i++;
			decodedBufferPos++;
		} else {
			decodedBuffer[decodedBufferPos] = encodedBuffer[i];
			decodedBufferPos++;
		}
	}

  int nonemptySize = decodedBufferPos;

  while (decodedBufferPos < decodedSize) {
    decodedBuffer[decodedBufferPos++] = 0;
  }

	return nonemptySize;

}
