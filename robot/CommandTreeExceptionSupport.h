/**
 * This class implements a tree made of commandNodes, each commandNode can have another
 * commandNode inside, or a pointer to a function. The tree has methods to navigate through
 * it, and obtain the data in the leaf nodes.
 */

#ifndef CommandTree_h
#define CommandTree_h

#include "Arduino.h"
#include "CommandNodeExceptionSupport.h"

class CommandTreeExceptionSupport {
private:

    CommandNodeExceptionSupport *_rootNode;
    CommandNodeExceptionSupport *_actualNode;

public:

	CommandTreeExceptionSupport();
	CommandTreeExceptionSupport(int subCommandsNumber);
	CommandTreeExceptionSupport(executable execute);
	void init(int subCommandsNumber);
    void toRootNode();
	void addSubCommand(const char *subCommandName);
	void addSubCommand(const char *subCommandName, int subCommandsNumber);
	void addSubCommand(const char *subCommandName, executable execute);
    void accessSubCommand(const char *subCommandName);
	int existsSubCommand(const char *subCommandName);
  bool isFunctionNode();
    const char *execute(const char *arguments);
	const char *test();

};

#endif /* CommandTree_h */
