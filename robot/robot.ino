/**
 * LIBRARIES
 */

#include <stdio.h>                          /* Standard input/output of C */
#include <string.h>                         /* Library containing functions to manipulate string elements in C */
#include "CommandTreeExceptionSupport.h"    /* Library that implements a tree dynamic structure */
#include <Wire.h>                           /* Library that implements I2C communications */
#include "StringSplitter.h"                 /* Implements tokenizing function for String objects */
#include "Braccio.h"                        /* Library that manages the movement of the TinkerKit Braccio */
#include "TinkerKit.h"                      /* Library that manages the data captured by the sensors */
#include <Servo.h>                          /* Library that implements the control over servomotors */
#include "Encoder.h"                        /* Library that implements a codification system */

/** 
 * CONSTANTS DEFINITION
 */

#define MPU 0x68                            /* Address of the MPU, used by the Wire class to establish the communication */

#define ARM_STEP 5
#define ARM_TIME_STEP 10

#define BAS_MIN_LIMIT 0
#define BAS_MAX_LIMIT 90
#define BAS_INIT_POS 45

#define SHL_MIN_LIMIT 90
#define SHL_MAX_LIMIT 140
#define SHL_INIT_POS 140

#define ELB_MIN_LIMIT 0
#define ELB_MAX_LIMIT 85
#define ELB_INIT_POS 0

#define WRV_MIN_LIMIT 0
#define WRV_MAX_LIMIT 90
#define WRV_INIT_POS 0

#define WRR_MIN_LIMIT 0
#define WRR_MAX_LIMIT 180
#define WRR_INIT_POS 0

#define GRP_MIN_LIMIT 10
#define GRP_MAX_LIMIT 73
#define GRP_INIT_POS 73

/**
 * POSITION STRUCT TYPEDEF AND FUNCTIONS
 */

typedef struct PositionStruct {
  int _currentPosition;
  int _maxLimit;
  int _minLimit;
} Position_t;

void initPosition(Position_t *positionT, int maxLimit, int minLimit) {
  positionT->_maxLimit = maxLimit;
  positionT->_minLimit = minLimit;
  positionT->_currentPosition = minLimit;
};

void incrementPosition(Position_t *positionT, int stepT) {
  if (positionT->_currentPosition < (positionT->_maxLimit) - stepT) {
    positionT->_currentPosition += stepT;
  } else {
    positionT->_currentPosition = positionT->_maxLimit;
  }
};

void decrementPosition(Position_t *positionT, int stepT) {
  if (positionT->_currentPosition > (positionT->_minLimit) + stepT) {
    positionT->_currentPosition -= stepT;
  } else {
    positionT->_currentPosition = positionT->_minLimit;
  }
};

void setPosition(Position_t *positionT, int absPosition) {
  if (absPosition < positionT->_minLimit) {
    positionT->_currentPosition = positionT->_minLimit;
  } else if (absPosition > positionT->_maxLimit) {
    positionT->_currentPosition = positionT->_maxLimit;
  } else {
    positionT->_currentPosition = absPosition;
  }
};

/**
 * FLOATASBYTES UNION TYPEDEF
 */

typedef union FloatAsBytes{
  float fval;
  byte bval[4];
} FloatAsBytes_u;

/**
 * GLOBAL VARIABLES DEFINITIONS
 */

Servo base, shoulder, elbow, wrist_ver, wrist_rot, gripper;     /* Instances of the Servo class used to control the servomotors */

const CommandTreeExceptionSupport commandTree PROGMEM;          /* Instance of the CommandTreeExceptionSupport class, used to control the tree */

TKLightSensor lightSensor(I1);              /* Instance of the TKLightSensor class of the TinkerKit library, used to read from the LDR */
TKThermistor thermistor(I0);                /* Instance of the TKThermistor class of the TinkerKit library, used to read from the thermistor */

byte output[15];                            /* Global buffer used to store data generated in the execution of a command received in the serial bus */
int usedOutput;                             /* Integer corresponding to the number of bytes used in the response generation */

bool processing;                            /* State variable used to define when the system is processing a command received */
char command[64];                           /* Buffer where commands are stored when being received */
byte counter;                               /* Variable where the number of used bytes in the command buffer is stored */
const Encoder encoder PROGMEM;              /* Instance of the Encoder class that defines the codification algorithm used in data transmission */

Position_t positionBase, positionShoulder, positionElbow, positionWristV, positionWristR, positionGripper;  /* These variables store the possitions of the different servos */

///////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// SETUP FUNCTION ////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

void setup () {

  Serial3.begin(9600);
  Serial.begin(9600);

  commandTree.init(5);                                      /* Root node has 5 descendant nodes */
  commandTree.addSubCommand("ARM", 7);                      /* ARM node has 7 descendant nodes */
  commandTree.addSubCommand("MEAS", 3);                     /* MEAS node has 3 descendant nodes */
  commandTree.addSubCommand("IDN", versionID);              /* IDN is leaf node associated to versionID function */
  commandTree.addSubCommand("RST", reset);                  /* RST is leaf node associated to reset function */
  commandTree.addSubCommand("DEMO", demo);                  /* DEMO is leaf node associated to demo function */

  commandTree.accessSubCommand("ARM");
  commandTree.addSubCommand("BAS", 2);                      /* BAS node has 2 descendant nodes */
  commandTree.addSubCommand("SHL", 2);                      /* SHL node has 2 descendant nodes */
  commandTree.addSubCommand("ELB", 2);                      /* ELB node has 2 descendant nodes */
  commandTree.addSubCommand("WRV", 2);                      /* WRV node has 2 descendant nodes */
  commandTree.addSubCommand("WRR", 2);                      /* WRR node has 2 descendant nodes */
  commandTree.addSubCommand("GRP", 2);                      /* GRP node has 2 descendant nodes */
  commandTree.addSubCommand("ABS", absolutePosition);       /* ABS is leaf node associated to absolutePosition function */

  commandTree.accessSubCommand("BAS");
  commandTree.addSubCommand("ADD", baseIncrement);          /* ADD is leaf node associated to baseIncrement function */
  commandTree.addSubCommand("SUS", baseDecrement);          /* SUS is leaf node associated to baseDecrement function */

  commandTree.toRootNode();
  commandTree.accessSubCommand("ARM");
  commandTree.accessSubCommand("SHL");
  commandTree.addSubCommand("ADD", shoulderIncrement);      /* ADD is leaf node associated to shoulderIncrement function */
  commandTree.addSubCommand("SUS", shoulderDecrement);      /* SUS is leaf node associated to shoulderDecrement function */

  commandTree.toRootNode();
  commandTree.accessSubCommand("ARM");
  commandTree.accessSubCommand("ELB");
  commandTree.addSubCommand("ADD", elbowIncrement);         /* ADD is leaf node associated to elbowIncrement function */
  commandTree.addSubCommand("SUS", elbowDecrement);         /* SUS is leaf node associated to elbowDecrement function */

  commandTree.toRootNode();
  commandTree.accessSubCommand("ARM");
  commandTree.accessSubCommand("WRV");
  commandTree.addSubCommand("ADD", wristVIncrement);        /* ADD is leaf node associated to wristVIncrement function */
  commandTree.addSubCommand("SUS", wristVDecrement);        /* SUS is leaf node associated to wristVDecrement function */

  commandTree.toRootNode();
  commandTree.accessSubCommand("ARM");
  commandTree.accessSubCommand("WRR");
  commandTree.addSubCommand("ADD", wristRIncrement);        /* ADD is leaf node associated to wristRIncrement function */
  commandTree.addSubCommand("SUS", wristRDecrement);        /* SUS is leaf node associated to wristRDecrement function */

  commandTree.toRootNode();
  commandTree.accessSubCommand("ARM");
  commandTree.accessSubCommand("GRP");
  commandTree.addSubCommand("ADD", gripperIncrement);       /* ADD is leaf node associated to gripperIncrement function */
  commandTree.addSubCommand("SUS", gripperDecrement);       /* SUS is leaf node associated to gripperDecrement function */
  
  commandTree.toRootNode();
  commandTree.accessSubCommand("MEAS");
//  commandTree.addSubCommand("ALL", mAllSensors);            /* ALL is leaf node associated to mAllSensors function */
  commandTree.addSubCommand("ACC", mAccelerometer);         /* ACC is leaf node associated to mAccelerometer function */
  commandTree.addSubCommand("TEMP", mTemperature);          /* TEMP is leaf node associated to mTemperature function */
  commandTree.addSubCommand("LUM", mLuminosity);            /* LUM is leaf node associated to mLuminosity function */

/**
 * MPU CONFIGURATION
 */

  pinMode(21, OUTPUT);
  pinMode(22, OUTPUT);

  Wire.begin();
  Wire.beginTransmission(MPU);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);

/**
 * ROBOT INITIAL POSITIONS
 */

  initPosition(&positionBase, BAS_MAX_LIMIT, BAS_MIN_LIMIT);
  initPosition(&positionShoulder, SHL_MAX_LIMIT, SHL_MIN_LIMIT);
  initPosition(&positionElbow, ELB_MAX_LIMIT, ELB_MIN_LIMIT);
  initPosition(&positionWristV, WRV_MAX_LIMIT, WRV_MIN_LIMIT);
  initPosition(&positionWristR, WRR_MAX_LIMIT, WRR_MIN_LIMIT);
  initPosition(&positionGripper, GRP_MAX_LIMIT, GRP_MIN_LIMIT);

  setPosition(&positionBase, BAS_INIT_POS);
  setPosition(&positionShoulder, SHL_INIT_POS);
  setPosition(&positionElbow, ELB_INIT_POS);
  setPosition(&positionWristV, WRV_INIT_POS);
  setPosition(&positionWristR, WRR_INIT_POS);
  setPosition(&positionGripper, GRP_INIT_POS);

/**
 * BRACCIO INITIALIZATION
 */

  Braccio.begin();
  Braccio.ServoMovement(ARM_TIME_STEP, positionBase._currentPosition,
                        positionShoulder._currentPosition,
                        positionElbow._currentPosition,
                        positionWristV._currentPosition,
                        positionWristR._currentPosition,
                        positionGripper._currentPosition);

/** 
 * INITIAL STATE
 */
                        
  processing = false;
  counter = 0;
  usedOutput = 0;

  pinMode(15, INPUT_PULLUP);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// LOOP FUNCTION ////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

void loop () {
  String commandString;
  if (!processing) {
    if (Serial3.available() > 0) {
      char data[1];
      Serial3.readBytes(data, 1);
      Serial.print(int(data), DEC);
      Serial.println();
      if (data[0] == '\n') {
        char command_trim[counter + 1];
        for (int i = 0; i < counter; i++) {
          command_trim[i] = command[i];
        }
        command_trim[counter] = '\0';
        commandString = String(command_trim);
        processing = true;
        counter = 0;
        execute(commandString);
      } else {
        if (counter >= 64) {
          counter = 0;
        } else {
          command[counter] = data[0];
          counter++;
        }
      }
    }
  } else {
    processing = false;
    counter = 0;
//    Serial.print("Procesado");
//    Serial3.write(COM_FLUSH);
//    Serial.print(output[0],DEC);
    if (output[0] >= COM_MAGIC) {
//      Serial.print(COM_DATA,DEC);
      Serial3.write(COM_DATA);
      for (int i = 0; i < usedOutput; i++) {
//        Serial.print(output[i],DEC);
        Serial3.write(output[i]);
      }
//      Serial.print(COM_END,DEC);
      Serial3.write(COM_END);
    }
    else if (output[0] == COM_ACK) {
//      Serial.print("IM here");
//      Serial.print(COM_ACK,DEC);
//      Serial.print(COM_END,DEC);
      Serial3.write(COM_ACK);
      Serial3.write(COM_END);
    } else if (output[0] == COM_ERR) {
//      Serial.write("ERR");
      Serial3.write(COM_ERR);
      Serial3.write(COM_END);
    };
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// EXECUTE FUNCTION ////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

const char *execute(String command) {
  StringSplitter *commandSplitter = new StringSplitter(command, ':', 4);                        /* The command string is splitted into n substrings separated by ':' */
  unsigned int nCommands = commandSplitter->getItemCount();
  String lastElement = commandSplitter->getItemAtIndex(nCommands - 1);
  commandTree.toRootNode();
  StringSplitter *commandArgumentSplitter = new StringSplitter(lastElement, ' ', 2);            /* The last substring is splitted into 2 substrings separated by ' ' */
  int nLastElement = commandArgumentSplitter->getItemCount();
  for (int i = 0; i < (nCommands - 1); i++) {                                                   /* The first n - 1 substrings are accessed if they exist in the command tree */
    if (commandTree.existsSubCommand(commandSplitter->getItemAtIndex(i).c_str())) {
      commandTree.accessSubCommand(commandSplitter->getItemAtIndex(i).c_str());
    } else {
      byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
      usedOutput = 1;
      for (int i = 0; i < 15; i++) {
        output[i] = error[i];
      }
      delete commandSplitter;
      delete commandArgumentSplitter;
      return 3;
    }
  }
  const char *arguments;
  if (nLastElement > 1) {                                                                       /* The existence of more than 1 substring at this step implies that there */                                                                                             /* are parameters to consider in the command to be executed */
    if (commandTree.existsSubCommand(commandArgumentSplitter->getItemAtIndex(0).c_str())) {  
      commandTree.accessSubCommand(commandArgumentSplitter->getItemAtIndex(0).c_str());
      arguments = malloc(strlen(commandArgumentSplitter->getItemAtIndex(1).c_str()) + 1);
      strcpy(arguments, commandArgumentSplitter->getItemAtIndex(1).c_str());
    } else {
      byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
      usedOutput = 1;
      for (int i = 0; i < 15; i++) {
        output[i] = error[i];
      }
      delete commandSplitter;
      delete commandArgumentSplitter;
      return 3;
    }
  } else if (strchr(commandArgumentSplitter->getItemAtIndex(0).c_str(), '?') != NULL) {         /* If the last element presents a '?' symbol, the command is of the query kind */
    StringSplitter *queryCommand = new StringSplitter(commandArgumentSplitter->getItemAtIndex(0), '?', 2);  /* The command name and the '?' are separated */
    if (commandTree.existsSubCommand(queryCommand->getItemAtIndex(0).c_str())) {
      commandTree.accessSubCommand(queryCommand->getItemAtIndex(0).c_str());
      delete queryCommand;
      arguments = "query";
    } else {
      byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
      usedOutput = 1;
      for (int i = 0; i < 15; i++) {
        output[i] = error[i];
      }
      delete queryCommand;
      delete commandSplitter;
      delete commandArgumentSplitter;
      return 3;
    }
  } else {                                                                                      /* If there is no query symbol and no parameters, the default command is */                                                                                             /* executed */
    if (commandTree.existsSubCommand(commandArgumentSplitter->getItemAtIndex(0).c_str())) {
      commandTree.accessSubCommand(commandArgumentSplitter->getItemAtIndex(0).c_str());
      arguments = "def";
    } else {
      byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
      usedOutput = 1;
      for (int i = 0; i < 15; i++) {
        output[i] = error[i];
      }
      delete commandSplitter;
      delete commandArgumentSplitter;
      return 3;
    }
  }
  delete commandSplitter;
  delete commandArgumentSplitter;
  if (commandTree.isFunctionNode()) {                                                           /* This last step checks if the command to be executed exists as a */
    return commandTree.execute(arguments);                                                      /* leaf node, to avoid random function pointers */
  } else {
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return NULL;
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// COMMAND FUNCTIONS ///////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

/** 
 * SYSTEM FUNCTIONS
 */

const char *versionID(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    byte response[4] = {'V', '1', '.', '1'};
    usedOutput = encoder.encodeData(response, 4, output, 15);
    Serial.print("Version");
    return 7;
  } else if (strcmp(arguments, "def") == 0) {
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return 3;
  } else {
    free(arguments);
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return 3;
  }
}

const char *reset(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return 3;
  } else if (strcmp(arguments, "def") == 0) {
    setPosition(&positionBase, BAS_INIT_POS);
    setPosition(&positionShoulder, SHL_INIT_POS);
    setPosition(&positionElbow, ELB_INIT_POS);
    setPosition(&positionWristV, WRV_INIT_POS);
    setPosition(&positionWristR, WRR_INIT_POS);
    setPosition(&positionGripper, GRP_INIT_POS);
    Braccio.ServoMovement(20, positionBase._currentPosition,
                          positionShoulder._currentPosition,
                          positionElbow._currentPosition,
                          positionWristV._currentPosition,
                          positionWristR._currentPosition,
                          positionGripper._currentPosition);
    byte ack [15] = {COM_ACK, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = ack[i];
    }
    return 3;
  } else {
    free(arguments);
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return 3;
  }
}

const char *demo(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return 3;
  } else if (strcmp(arguments, "def") == 0) {
    byte error [15] = {252, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return 3;
  } else {
    StringSplitter *argumentSplitter = new StringSplitter(arguments, ' ', 1);
    const char *argument = malloc(argumentSplitter->getItemAtIndex(0).c_str() + 1);
    strcpy(argument, argumentSplitter->getItemAtIndex(0).c_str());
    int nDemo = atoi(argument);
    delete argumentSplitter;
    if (nDemo == 1) {
      Braccio.ServoMovement(20,           45,  140, 0, 0,  0,  73);
      for (int i = 0; i < 5; i++) {
        Braccio.ServoMovement(20,           45,  140, 45, 50,  0,  15);
        delay(250);

        Braccio.ServoMovement(20,     45,  140,  45,   50,  0,   73);
        delay(250);
      }
      setPosition(&positionBase, BAS_INIT_POS);
      setPosition(&positionShoulder, SHL_INIT_POS);
      setPosition(&positionElbow, ELB_INIT_POS);
      setPosition(&positionWristV, WRV_INIT_POS);
      setPosition(&positionWristR, WRR_INIT_POS);
      setPosition(&positionGripper, GRP_INIT_POS);
      Braccio.ServoMovement(20, positionBase._currentPosition,
                            positionShoulder._currentPosition,
                            positionElbow._currentPosition,
                            positionWristV._currentPosition,
                            positionWristR._currentPosition,
                            positionGripper._currentPosition);
    } else if (nDemo == 2) {
      Braccio.ServoMovement(20, 45, 140, 0, 0, 90, 73);
      delay(100);
      Braccio.ServoMovement(20, 10, 140, 10, 0, 0, 73);
      Braccio.ServoMovement(20, 45, 140, 0, 0, 90, 73);
      Braccio.ServoMovement(20, 80, 140, 10, 0, 180, 73);
      Braccio.ServoMovement(20, 45, 90, 90, 90, 0, 15);
      setPosition(&positionBase, BAS_INIT_POS);
      setPosition(&positionShoulder, SHL_INIT_POS);
      setPosition(&positionElbow, ELB_INIT_POS);
      setPosition(&positionWristV, WRV_INIT_POS);
      setPosition(&positionWristR, WRR_INIT_POS);
      setPosition(&positionGripper, GRP_INIT_POS);
      Braccio.ServoMovement(20, positionBase._currentPosition,
                            positionShoulder._currentPosition,
                            positionElbow._currentPosition,
                            positionWristV._currentPosition,
                            positionWristR._currentPosition,
                            positionGripper._currentPosition);
    } else if (nDemo == 3) {
      Braccio.ServoMovement(20, 45, 140, 0, 0, 0, 73);
      Braccio.ServoMovement(20, 0, 90, 0, 90, 0, 73);
      Braccio.ServoMovement(20, 180, 90, 0, 90, 0, 73);
      setPosition(&positionBase, BAS_INIT_POS);
      setPosition(&positionShoulder, SHL_INIT_POS);
      setPosition(&positionElbow, ELB_INIT_POS);
      setPosition(&positionWristV, WRV_INIT_POS);
      setPosition(&positionWristR, WRR_INIT_POS);
      setPosition(&positionGripper, GRP_INIT_POS);
      Braccio.ServoMovement(20, positionBase._currentPosition,
                            positionShoulder._currentPosition,
                            positionElbow._currentPosition,
                            positionWristV._currentPosition,
                            positionWristR._currentPosition,
                            positionGripper._currentPosition);
    }
    free(argument);
    free(arguments);
    byte ack [15] = {COM_ACK, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = ack[i];
    }
    return 3;
  }
}

/**
 * BASE FUNCTIONS 
 */
 
const char *
(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    byte base1 = (byte) ((positionBase._currentPosition && 0xFF00) >> 8);
    byte base2 = (byte) (positionBase._currentPosition && 0x00FF);
    byte response[2] = {base1, base2};
    usedOutput = encoder.encodeData(response, 2, output, 15);
    return 5;
  } else if (strcmp(arguments, "def") == 0) {
    incrementPosition(&positionBase, ARM_STEP);
    Braccio.ServoMovement(ARM_TIME_STEP, positionBase._currentPosition,
                          positionShoulder._currentPosition,
                          positionElbow._currentPosition,
                          positionWristV._currentPosition,
                          positionWristR._currentPosition,
                          positionGripper._currentPosition);
    byte ack [15] = {COM_ACK, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = ack[i];
    }
    return 3;
  } else {
    free(arguments);
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return 3;
  }
}

const char *baseDecrement(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    byte base1 = (byte) ((positionBase._currentPosition && 0xFF00) >> 8);
    byte base2 = (byte) (positionBase._currentPosition && 0x00FF);
    byte response[2] = {base1, base2};
    usedOutput = encoder.encodeData(response, 2, output, 15);
    return 5;
  } else if (strcmp(arguments, "def") == 0) {
    decrementPosition(&positionBase, ARM_STEP);
    Braccio.ServoMovement(ARM_TIME_STEP, positionBase._currentPosition,
                          positionShoulder._currentPosition,
                          positionElbow._currentPosition,
                          positionWristV._currentPosition,
                          positionWristR._currentPosition,
                          positionGripper._currentPosition);
    byte ack [15] = {COM_ACK, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = ack[i];
    }
    return 3;
  } else {
    free(arguments);
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  }
}

/**
 * SHOULDER FUNCTIONS
 */

const char *shoulderIncrement(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    byte shoulder1 = (byte) ((positionShoulder._currentPosition && 0xFF00) >> 8);
    byte shoulder2 = (byte) (positionShoulder._currentPosition && 0x00FF);
    byte response[2] = {shoulder1, shoulder2};
    usedOutput = encoder.encodeData(response, 2, output, 15);
    return 5;
  } else if (strcmp(arguments, "def") == 0) {
    incrementPosition(&positionShoulder, ARM_STEP);
    Braccio.ServoMovement(ARM_TIME_STEP, positionBase._currentPosition,
                          positionShoulder._currentPosition,
                          positionElbow._currentPosition,
                          positionWristV._currentPosition,
                          positionWristR._currentPosition,
                          positionGripper._currentPosition);
    byte ack [15] = {COM_ACK, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = ack[i];
    }
    return 3;
  } else {
    free(arguments);
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  }
}

const char *shoulderDecrement(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    byte shoulder1 = (byte) ((positionShoulder._currentPosition && 0xFF00) >> 8);
    byte shoulder2 = (byte) (positionShoulder._currentPosition && 0x00FF);
    byte response[2] = {shoulder1, shoulder2};
    usedOutput = encoder.encodeData(response, 2, output, 15);
    return "ok";
  } else if (strcmp(arguments, "def") == 0) {
    decrementPosition(&positionShoulder, ARM_STEP);
    Braccio.ServoMovement(ARM_TIME_STEP, positionBase._currentPosition,
                          positionShoulder._currentPosition,
                          positionElbow._currentPosition,
                          positionWristV._currentPosition,
                          positionWristR._currentPosition,
                          positionGripper._currentPosition);
    byte ack [15] = {COM_ACK, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = ack[i];
    }
    return "ok";
  } else {
    free(arguments);
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  }
}

/**
 * ELBOW FUNCTIONS
 */

const char *elbowIncrement(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    byte elbow1 = (byte) ((positionElbow._currentPosition && 0xFF00) >> 8);
    byte elbow2 = (byte) (positionElbow._currentPosition && 0x00FF);
    byte response[2] = {elbow1, elbow2};
    usedOutput = encoder.encodeData(response, 2, output, 15);
    return 5;
  } else if (strcmp(arguments, "def") == 0) {
    incrementPosition(&positionElbow, ARM_STEP);
    Braccio.ServoMovement(ARM_TIME_STEP, positionBase._currentPosition,
                          positionShoulder._currentPosition,
                          positionElbow._currentPosition,
                          positionWristV._currentPosition,
                          positionWristR._currentPosition,
                          positionGripper._currentPosition);
    byte ack [15] = {COM_ACK, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = ack[i];
    }
    return 3;
  } else {
    free(arguments);
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  }
}

const char *elbowDecrement(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    byte elbow1 = (byte) ((positionElbow._currentPosition && 0xFF00) >> 8);
    byte elbow2 = (byte) (positionElbow._currentPosition && 0x00FF);
    byte response[2] = {elbow1, elbow2};
    usedOutput = encoder.encodeData(response, 2, output, 15);
    return "ok";
  } else if (strcmp(arguments, "def") == 0) {
    decrementPosition(&positionElbow, ARM_STEP);
    Braccio.ServoMovement(ARM_TIME_STEP, positionBase._currentPosition,
                          positionShoulder._currentPosition,
                          positionElbow._currentPosition,
                          positionWristV._currentPosition,
                          positionWristR._currentPosition,
                          positionGripper._currentPosition);
    byte ack [15] = {COM_ACK, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = ack[i];
    }
    return "ok";
  } else {
    free(arguments);
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  }
}

/**
 * WRIST VERTICAL FUNCTIONS
 */

const char *wristVIncrement(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    byte wristv1 = (byte) ((positionWristV._currentPosition && 0xFF00) >> 8);
    byte wristv2 = (byte) (positionWristV._currentPosition && 0x00FF);
    byte response[2] = {wristv1, wristv2};
    usedOutput = encoder.encodeData(response, 2, output, 15);
    return 5;
  } else if (strcmp(arguments, "def") == 0) {
    incrementPosition(&positionWristV, ARM_STEP);
    Braccio.ServoMovement(ARM_TIME_STEP, positionBase._currentPosition,
                          positionShoulder._currentPosition,
                          positionElbow._currentPosition,
                          positionWristV._currentPosition,
                          positionWristR._currentPosition,
                          positionGripper._currentPosition);
    byte ack [15] = {COM_ACK, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = ack[i];
    }
    return 3;
  } else {
    free(arguments);
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  }
}

const char *wristVDecrement(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    byte wristv1 = (byte) ((positionWristV._currentPosition && 0xFF00) >> 8);
    byte wristv2 = (byte) (positionWristV._currentPosition && 0x00FF);
    byte response[2] = {wristv1, wristv2};
    usedOutput = encoder.encodeData(response, 2, output, 15);
    return "ok";
  } else if (strcmp(arguments, "def") == 0) {
    decrementPosition(&positionWristV, ARM_STEP);
    Braccio.ServoMovement(ARM_TIME_STEP, positionBase._currentPosition,
                          positionShoulder._currentPosition,
                          positionElbow._currentPosition,
                          positionWristV._currentPosition,
                          positionWristR._currentPosition,
                          positionGripper._currentPosition);
    byte ack [15] = {COM_ACK, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = ack[i];
    }
    return "ok";
  } else {
    free(arguments);
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  }
}

/**
 * WRIST ROTATIONAL FUNCTIONS
 */

const char *wristRIncrement(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    byte wristr1 = (byte) ((positionWristR._currentPosition && 0xFF00) >> 8);
    byte wristr2 = (byte) (positionWristR._currentPosition && 0x00FF);
    byte response[2] = {wristr1, wristr2};
    usedOutput = encoder.encodeData(response, 2, output, 15);
    return 5;
  } else if (strcmp(arguments, "def") == 0) {
    incrementPosition(&positionWristR, ARM_STEP);
    Braccio.ServoMovement(ARM_TIME_STEP, positionBase._currentPosition,
                          positionShoulder._currentPosition,
                          positionElbow._currentPosition,
                          positionWristV._currentPosition,
                          positionWristR._currentPosition,
                          positionGripper._currentPosition);
    byte ack [15] = {COM_ACK, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = ack[i];
    }
    return 3;
  } else {
    free(arguments);
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  }
}

const char *wristRDecrement(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    byte wristr1 = (byte) ((positionWristR._currentPosition && 0xFF00) >> 8);
    byte wristr2 = (byte) (positionWristR._currentPosition && 0x00FF);
    byte response[2] = {wristr1, wristr2};
    usedOutput = encoder.encodeData(response, 2, output, 15);
    return "ok";
  } else if (strcmp(arguments, "def") == 0) {
    decrementPosition(&positionWristR, ARM_STEP);
    Braccio.ServoMovement(ARM_TIME_STEP, positionBase._currentPosition,
                          positionShoulder._currentPosition,
                          positionElbow._currentPosition,
                          positionWristV._currentPosition,
                          positionWristR._currentPosition,
                          positionGripper._currentPosition);
    byte ack [15] = {COM_ACK, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = ack[i];
    }
    return "ok";
  } else {
    free(arguments);
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  }
}

/**
 * GRIPPER FUNCTIONS
 */

const char *gripperIncrement(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    byte gripper1 = (byte) ((positionGripper._currentPosition && 0xFF00) >> 8);
    byte gripper2 = (byte) (positionGripper._currentPosition && 0x00FF);
    byte response[2] = {gripper1, gripper2};
    usedOutput = encoder.encodeData(response, 2, output, 15);
    return 5;
  } else if (strcmp(arguments, "def") == 0) {
    incrementPosition(&positionGripper, ARM_STEP);
    Braccio.ServoMovement(ARM_TIME_STEP, positionBase._currentPosition,
                          positionShoulder._currentPosition,
                          positionElbow._currentPosition,
                          positionWristV._currentPosition,
                          positionWristR._currentPosition,
                          positionGripper._currentPosition);
    byte ack [15] = {COM_ACK, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = ack[i];
    }
    return 3;
  } else {
    free(arguments);
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  }
}

const char *gripperDecrement(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    byte gripper1 = (byte) ((positionGripper._currentPosition && 0xFF00) >> 8);
    byte gripper2 = (byte) (positionGripper._currentPosition && 0x00FF);
    byte response[2] = {gripper1, gripper2};
    usedOutput = encoder.encodeData(response, 2, output, 15);
    return "ok";
  } else if (strcmp(arguments, "def") == 0) {
    decrementPosition(&positionGripper, ARM_STEP);
    Braccio.ServoMovement(ARM_TIME_STEP, positionBase._currentPosition,
                          positionShoulder._currentPosition,
                          positionElbow._currentPosition,
                          positionWristV._currentPosition,
                          positionWristR._currentPosition,
                          positionGripper._currentPosition);
    byte ack [15] = {COM_ACK, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = ack[i];
    }
    return "ok";
  } else {
    free(arguments);
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  }
}

/**
 * ABSOLUTE POSITION FUNCTION
 */

const char *absolutePosition(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  } else if (strcmp(arguments, "def") == 0) {
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  } else {
    StringSplitter *argumentSplitter = new StringSplitter(arguments, ' ', 6);
    const char *argument = malloc(argumentSplitter->getItemAtIndex(0).c_str() + 1);
    if (argumentSplitter->getItemCount() == 6) {
      strcpy(argument, argumentSplitter->getItemAtIndex(0).c_str());
      int posBase = atoi(argument);
      setPosition(&positionBase, posBase);
      strcpy(argument, argumentSplitter->getItemAtIndex(1).c_str());
      int posShoulder = atoi(argument);
      setPosition(&positionShoulder, posShoulder);
      strcpy(argument, argumentSplitter->getItemAtIndex(2).c_str());
      int posElbow = atoi(argument);
      setPosition(&positionElbow, posElbow);
      strcpy(argument, argumentSplitter->getItemAtIndex(3).c_str());
      int posWristV = atoi(argument);
      setPosition(&positionWristV, posWristV);
      strcpy(argument, argumentSplitter->getItemAtIndex(4).c_str());
      int posWristR = atoi(argument);
      setPosition(&positionWristR, posWristR);
      strcpy(argument, argumentSplitter->getItemAtIndex(5).c_str());
      int posGripper = atoi(argument);
      setPosition(&positionGripper, posGripper);
      delete argumentSplitter;
      Braccio.ServoMovement(10, positionBase._currentPosition,
                            positionShoulder._currentPosition,
                            positionElbow._currentPosition,
                            positionWristV._currentPosition,
                            positionWristR._currentPosition,
                            positionGripper._currentPosition);
      free(argument);
      free(arguments);
      byte ack [15] = {COM_ACK, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
      usedOutput = 1;
      for (int i = 0; i < 15; i++) {
        output[i] = ack[i];
      }
      return "ok";
    } else  {
      byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
      usedOutput = 1;
      for (int i = 0; i < 15; i++) {
        output[i] = error[i];
      }
      return "ndef";
    }
  }
}

/**
 * MEASURING FUNCTIONS
 */

const char *mAccelerometer(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    int16_t GyX, GyY;
    Wire.beginTransmission(MPU);
    Wire.write(0x3B);
    Wire.endTransmission(false);
    Wire.requestFrom(MPU, 6, true);
    byte GyX1 = Wire.read();
    byte GyX2 = Wire.read();
    byte GyY1 = Wire.read();
    byte GyY2 = Wire.read();
    byte AcZ1 = Wire.read();
    byte AcZ2 = Wire.read();
    byte response[6] = {GyX1, GyX2, GyY1, GyY2, AcZ1, AcZ2};
    usedOutput = encoder.encodeData(response, 6, output, 15);
//    Serial.println("");
//    Serial.println("ACELL");
//    Serial.print(GyX1, DEC);
//    Serial.print(GyX2, DEC);
//    Serial.print(GyY1, DEC);
//    Serial.print(GyY2, DEC);
//    Serial.println("");
    return "ok";
  } else if (strcmp(arguments, "def") == 0) {
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  } else {
    free(arguments);
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  }
}

const char *mTemperature(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    Serial.println("Temperatura");
    FloatAsBytes_u temp;
    temp.fval = thermistor.readCelsius();
    Serial.print(temp.fval);
    Serial.println("");
    Serial.println("Raw Data> ");
    byte response[4] = {temp.bval[0], temp.bval[1], temp.bval[2], temp.bval[3]};
    for(int i =0; i<4; i++){
      Serial.print(response[i],DEC);
      Serial.print(" , ");
    }
    Serial.println("");
    Serial.println("Encoded Data> ");
    usedOutput = encoder.encodeData(response, 4, output, 15);
    for(int i = 0; i<15; i++){
      Serial.print(output[i],DEC);
      Serial.print(" , ");
    }
    Serial.println("");
    return "ok";
  } else if (strcmp(arguments, "def") == 0) {
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  } else {
    free(arguments);
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  }
}

const char *mLuminosity(const char *arguments) {
  if (strcmp(arguments, "query") == 0) {
    int lum;
    lum = lightSensor.read();
    byte lum1 = (byte) ((lum & 0xFF00) >> 8);
    byte lum2 = (byte) (lum & 0x00FF);
    byte response[2] = {lum1, lum2};
    Serial.println("Luminosidad");
//    Serial.print(lum);
    for(int i =0; i<2; i++){
      Serial.print(response[i],DEC);
    }
    Serial.println("");
    usedOutput = encoder.encodeData(response, 2, output, 15);
    return "ok";
  } else if (strcmp(arguments, "def") == 0) {
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  } else {
    free(arguments);
    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    usedOutput = 1;
    for (int i = 0; i < 15; i++) {
      output[i] = error[i];
    }
    return "ndef";
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////// MISC /////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

//int availableMemory() {
//
//  int size = 8192;
//  byte *buf;
//  while ((buf = (byte *) malloc(--size)) == NULL);
//  free(buf);
//  return size;
//
//}

//const char *mAllSensors(const char *arguments) {
//
//  if (strcmp(arguments, "query") == 0) {
//
//    float temp;
//
//    temp = thermistor.readCelsius();
//    char buffer[10];
//    dtostrf(temp, 1, 1, buffer);
//
//    int lum;
//
//    lum = lightSensor.read();
//
//    int16_t AcX, AcY, AcZ;
//
//    Wire.beginTransmission(MPU);
//    Wire.write(0x3B);
//    Wire.endTransmission(false);
//    Wire.requestFrom(MPU, 6, true);
//    AcX = (Wire.read() << 8) | (Wire.read());
//    AcY = (Wire.read() << 8) | (Wire.read());
//    AcZ = (Wire.read() << 8) | (Wire.read());
//
//    sprintf(output, "%s %d %d %d %d", buffer, lum, AcX, AcY, AcZ);
//
//    return "ok";
//
//  } else if (strcmp(arguments, "def") == 0) {
//
//    /* The code refers to a command error, the command specified has not been found */
//    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
//    usedOutput = 1;
//    for (int i = 0; i < 15; i++) {
//      output[i] = error[i];
//    }
//
//    return "ndef";
//
//  } else {
//
//    free(arguments);
//
//    /* The code refers to a command error, the command specified has not been found */
//    byte error [15] = {COM_ERR, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
//    usedOutput = 1;
//    for (int i = 0; i < 15; i++) {
//      output[i] = error[i];
//    }
//
//    return "ndef";
//
//  }
//
//}
