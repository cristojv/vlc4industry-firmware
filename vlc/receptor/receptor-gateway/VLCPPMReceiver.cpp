/*
   VLCPPMReceiver.cpp
   Software vlc library for Arduino using PPM modulation.
   Caracteristics:
   -> Interrupt-driven receiver
   -> Interrupt-driven transmitter
   -> ATmega 2560 support
*/

/* Tools for Debugging
    When set, _DEBUG, it will be used pins 11 and 13 for debuggin with an
    oscilloscope or logic analyzer.
*/
#include "Arduino.h"
#include "VLCPPMReceiver.h"
#include "avr/pgmspace.h"

VLCPPMReceiver *VLCPPMReceiver::active_VLCPPMReceiver = 0;

/*
  =======================================================================================
  Private functions
  =======================================================================================
*/

/*****************************************************************************************
  Pin functions
*****************************************************************************************/
void VLCPPMReceiver::setReceiverPin(uint8_t receiverPin) {
  pinMode(receiverPin, INPUT);
  digitalWrite(receiverPin, HIGH);
  isReceiving = false;
}

void VLCPPMReceiver::initIndicatorPins(uint8_t isCalibratedIndicatorPin) {
  pinMode(isCalibratedIndicatorPin,OUTPUT);
  digitalWrite(isCalibratedIndicatorPin,LOW);
}

/*****************************************************************************************
  Timer functions
*****************************************************************************************/

bool VLCPPMReceiver::configureTimerFor(float chipFrecuency) {

  /*Initial considerations
     CPU_CLOCK = 16MHz;
     Timer resolution = 16 bits;
  /*
   * Procedure.
     1)Disabling interruptions. Atomic operation space.
     2)Clearing timer registers.
     3)Initialize timer variables for counting.
     4)Select prescaler and chipCount.
     5)Check if a correct value was selected for prescaler and chipCount.
     6)Configure timer. Timer must work on Normal mode of operation
     7)Set global count variables (headerCount, chipCount, erroCountCorrection)
  */
  noInterrupts();

  TCCR3A = 0;
  TCCR3B = 0;
  TCNT3 = 0;

  int prescaler = 8;
  float chipCountFloat = 0.0;

  /* prescalerMaskRegister -> Represent the mask of the selected prescaler, used
                              during the configuration of the time registers.
                              Ej: for prescaler[8,64,256,1024] -> maskRegister [2,3,4,5]
  */
  uint8_t prescalerMaskRegister;


  for (prescalerMaskRegister = 2; prescalerMaskRegister <= 5; prescalerMaskRegister++) {

    chipCountFloat = 16000000 / (chipFrecuency * prescaler);

    //If the chipCount is less than 2^16 (The timer has 16 bit resolution)
    //Then continue, if not, increase prescaler and try again.

    if (chipCountFloat < 65536) {
      break;
    };
    prescaler = prescaler * 8;
  }

  if (chipCountFloat >= 65536) {
    //NO SUCCESSFUL CONFIGURATION
    return 0;
  }
  
  //NORMAL Mode of Operation
  TCCR3A = 0;
  TCCR3B &= ~(1 << WGM32);
  TCCR3B &= ~(1 << WGM33);

  //PRESCALER
  TCCR3B |= prescalerMaskRegister;
  
  chipCount = round(chipCountFloat);
  computeTimingCounts(chipCount);
  
  //Enable interrupts
  interrupts();
  
  //Successful configuration
  return 1;
}
void VLCPPMReceiver::computeTimingCounts(uint16_t chipCount){
  
  headerCount = round(VLC_HEADCHIPS * chipCount);
  tailCount = round(VLC_TAILCHIPS * chipCount);
  errorCount = round(VLC_ERRORCHIPS * chipCount);
  idleCount = round(VLC_IDLECHIPS * chipCount);
  errorCountCorrection = round(chipCount * errorPercentage);

//  Serial.println("TIMING CONFIGURATION");
//  Serial.print("Chip count -> ");
//  Serial.println(chipCount);
//  Serial.println();
//  Serial.print("Header count -> ");
//  Serial.println(headerCount);
//  Serial.print("Tail count -> ");
//  Serial.println(tailCount);
//  Serial.print("Error count -> ");
//  Serial.println(errorCount);
//  Serial.print("Error count -> ");
//  Serial.println(errorCountCorrection);
//  Serial.print("Idle count -> ");
//  Serial.println(idleCount);
}


/*****************************************************************************************
  Demodulation functions
*****************************************************************************************/
uint8_t VLCPPMReceiver::demodulateData() {

//  if (isAvailableForDemodulation) {
    uint16_t currentDelay = 0;
    uint8_t charByte = 0;
    /*
       iteration -> Must be three cause the byte is splited in 4 groups
                    of 2 bits. The iteration is used for shifting the
                    groups of bits to the correspond position inside
                    the final byte.
                    Example: (11<<iteration * 2)
                    11 << 3 * 2 -> B-11-00-00-00
                    11 << 2 * 2 -> B-00-11-00-00
                    11 << 1 * 2 -> B-00-00-11-00
                    11 << 0 * 2 -> B-00-00-00-11
    */

    uint8_t bits = 0;
    int iteration = 3;
    bits = 0;
    charByte = 0;

    while (delaysBuffer.pop(&currentDelay)) {
      bits = 0;
      getBitsForDelay(currentDelay, &bits);
      charByte |= bits << (iteration * 2);

      if (iteration == 0) {
        charBuffer.push(charByte);
        iteration = 3;
        charByte = 0;
      } else {
        iteration = iteration - 1;
      }
    }

    /***********************************************************************
       Get Available for reading state.
     ***********************************************************************/
    return 1;
}

uint16_t VLCPPMReceiver::getTimeDelay() {
  long l = (TCNT3L);
  long h = (TCNT3H << 8);
  long currentTime = h | l;
  long delayTime = 0;
  if (currentTime < oldTime) {
    delayTime = 65534 - oldTime + currentTime;
  } else {
    delayTime = currentTime - oldTime;
  }
  oldTime = currentTime;
  return delayTime;
}

uint8_t VLCPPMReceiver::getBitsForDelay(uint16_t delayTime, uint8_t *pointerBits) {
  int delayCount = (int)round(delayTime * 1.0 / chipCount);
  //Serial.println(delayCount);
  switch (delayCount) {
    case 5:
      *pointerBits |= (B00000000);
      break;
    case 2:
    case 6:
      *pointerBits |= (B00000001);
      break;
    case 3:
    case 7:
      *pointerBits |= (B00000010);
      break;
    case 4:
    case 8:
      *pointerBits |= (B00000011);
      break;
    default:
      *pointerBits |= (B00000000);
  }
  return 1;
}

/*****************************************************************************************
  State functions
*****************************************************************************************/

/*
  =======================================================================================
  Public functions
  =======================================================================================
*/

/* CONSTRUCTOR & DESTRUCTOR
   @Params:
    receiverPin:  Pin for receiving data. (It's necesary that this pin allows external
                  interruptions.

    isInverted:   Inversion of the pulse signal (if Inverted -> signal on low logic level)

*/

VLCPPMReceiver::VLCPPMReceiver(uint8_t receiverPin, float errorPercentageOnSymbol, uint8_t isInverted) {
  initIndicatorPins(PIN_ISCALIBRATED_INDICATOR);
  setReceiverPin(receiverPin);
  errorPercentage = errorPercentageOnSymbol;
  rx_state = INIT; /**INIT State*/
}

VLCPPMReceiver::~VLCPPMReceiver() {
  end();
}
/*****************************************************************************************
  Reception functions
*****************************************************************************************/
boolean VLCPPMReceiver::available() {
  return isAvailable;
}

uint8_t VLCPPMReceiver::read(uint8_t *byteData) {
  if (isAvailable) {
      isAvailable = charBuffer.pop(byteData);
      return isAvailable;
  }
}

volatile uint8_t isDelayDetected = false;
void VLCPPMReceiver::recv() {
  cli();
  //Serial.println(delayTime,DEC);
  delayTime = getTimeDelay();
  isDelayDetected = true;
  switch (rx_state){
    
    case LISTENING:
      //Look for header
      //Calibrate delayTime
      if (headerCount - errorCountCorrection * 20 < delayTime && delayTime < headerCount + errorCountCorrection * 20){
        rx_state = RECEIVING;
      }
    break;

    case RECEIVING:
      //Look for tail
      
      if (tailCount - errorCountCorrection * 20 < delayTime && delayTime < tailCount + errorCountCorrection * 50) {
        rx_state = WAITINGFOR_DEMODULATION;
      }else if(delayTime > errorCount){
        uint16_t dataIn;
        while(delaysBuffer.pop(&dataIn));
        rx_state = LISTENING;
      }else{
        delaysBuffer.push(delayTime);
      }
      break;

    case WAITINGFOR_DEMODULATION:
      //Discard packets
      break;
  }
  sei();
}

void VLCPPMReceiver::setIndicatorCalibrated(bool isCalibrated){
  digitalWrite(PIN_ISCALIBRATED_INDICATOR,isCalibrated);
}
/*****************************************************************************************
  State functions
*****************************************************************************************/


void VLCPPMReceiver::begin(long frecuency) {
  active_VLCPPMReceiver = this;
  configureTimerFor(frecuency);
}

void VLCPPMReceiver::end() {
  if (active_VLCPPMReceiver == this) {
    active_VLCPPMReceiver = NULL;
  }
  rx_state = INIT;
}
void VLCPPMReceiver::startListening() {
  noInterrupts();
  //Configure PCINT5 interruption for falling edge
  EICRB |= (1 << ISC51);
  EICRB &= ~(1 << ISC50);
  //Enable interrupt
  EIMSK |= 1 << INT5;
  interrupts();
  rx_state = LISTENING;
}
void VLCPPMReceiver::stopListening() {
  noInterrupts();
  //Configure PCINT5 interruption for falling edge
  EICRB |= 1 << ISC51;
  EICRB &= ~(1 << ISC50);
  //Disable interrupt
  EIMSK &= ~(1 << INT5);
  interrupts();
}


inline void VLCPPMReceiver::handle_interrupt() {
  if (active_VLCPPMReceiver) {
    active_VLCPPMReceiver->recv();
  }
}

void VLCPPMReceiver::workOnLoop(){
  if(rx_state == WAITINGFOR_DEMODULATION){
    if (active_VLCPPMReceiver) {
//      Serial.println(charBuffer.getCount());
      if(charBuffer.getCount() == 0){
        uint8_t result = active_VLCPPMReceiver->demodulateData();
//        Serial.println(result);
        if(result == 1){
          isAvailable = true;
          if(delaysBuffer.getCount() == 0){//Double check.
            rx_state = LISTENING;
          }
        }else if(result == 0){
          //flush data
          //Islistening
        }else{
          
        }
      }
    }
  }else if(rx_state == LISTENING){
//    Serial.println("LISTENING");
//    Serial.println(delayTime);
      setIndicatorCalibrated(false);
      if(isDelayDetected){
        if(((VLC_IDLECHIPS-2) * chipCount)< delayTime && delayTime < ((VLC_IDLECHIPS+2) * chipCount)){ //TODO: Change 2 to be modified
              float difference = float(delayTime/VLC_IDLECHIPS - float(chipCount));
    //          Serial.print("Difference :");
//              Serial.println(difference);
              if(abs(difference)<1){
                setIndicatorCalibrated(true);
              }else{
    //            setIndicatorCalibrated(false);
                if(difference<0){
                chipCount--;
                }else if(difference>0){
                chipCount++;
                }
    //            Serial.print("ChipCount: " );
    //            Serial.println(chipCount);
                computeTimingCounts(chipCount);
              }
              isDelayDetected = false;
        }

          
  }}
}

ISR(INT5_vect) { //times compare interrupt service routine
  VLCPPMReceiver::handle_interrupt();
}
