/**
 * \file receptor-gateway.ino
 * \version 1.2
 * \brief VLC receptor acting as a gateway.
 * \author Cristo Manuel Jurado Verdu
 * \company IDeTIC
 * 
 * \characteristics: 1.1 C++11 standard and above
 *                   1.2 No exceptions, RTTI, virtual functions and dynamic memory allocation
 *                   1.3 No wasted slots
 *                   1.4 Lock-free atomic operation in SPSC cases
 *                   1.5 Underrun and overrun checks in insert/remove functions
 *                   1.6 High efficient on most microcontroller architectures
*/

#include "Arduino.h"
#include "VLCPPMReceiver.h"

/**
 * Definitions
 */

#define serialTimeout 1500
#define COM_ERR 0
#define COM_ACK 1
#define COM_DATA 2
#define COM_END 3
#define COM_FLUSH 4
#define COM_MAGIC 5

VLCPPMReceiver vlcReceiver(VLC_RECEIVER_PIN, 0.02, 1);

uint8_t i = HIGH;
uint8_t iteration = 0;

enum GATEWAY_STATE{
  INIT,
  LISTENING,
  COM_ROBOT,
  COM_LAMP,
  };

GATEWAY_STATE gatewayState = INIT;

typedef struct HistoryLog{
  uint8_t _id;
  uint8_t _response[20];
  uint8_t _responseSize;
}HistoryLog_t;

void initHistoryLog(HistoryLog_t *historyLog, uint8_t id, uint8_t response[], uint8_t responseSize){
  historyLog->_id = id;
  for(int i=0; i< responseSize; i++){
    historyLog->_response[i] = response[i];
  }
  historyLog->_responseSize = responseSize;
};

void setHistoryLogID(HistoryLog_t *historyLog, uint8_t id){
  historyLog->_id = id;
}
void setHistoryLogResponse(HistoryLog_t *historyLog, uint8_t *response[], uint8_t responseSize){
  for(int i=0; i< responseSize; i++){
    historyLog->_response[i] = response[i];
  }
  historyLog->_responseSize = responseSize;
}

HistoryLog_t historyLog;
uint8_t g_id = 5;
String g_command = "";
uint8_t g_response[20];
uint8_t g_responseSize;

void setup() {
  /**
   * INITIALIZATION OF SERIALS:
   * Serial1 -> Communication with Robot
   * Serial2 -> Communication with Lamp
   */
//  Serial.begin(115200);
  Serial1.setTimeout(serialTimeout);
  Serial2.setTimeout(serialTimeout);
  /**
   * INITIALIZATION OF VARIABLES:
   */
  uint8_t response[15];
  initHistoryLog(&historyLog, 5, response, 15);
  gatewayState = INIT;
  vlcReceiver.begin(40000);
}
  
void loop() {
  /**
   * GATEWAY - STATE Machine
   */
  switch(gatewayState){
    case INIT:
    /**
     * The Gateway will arrive here just once. In this state
     * configures the internal state.
     * 1) Closes Serial Inputs to avoid miscommunications
     * 2) Flushes Serial Inputs to asure clean communication
     * 3) Clear internal variables:
     *    command
     *    response
     *    Except for HistoryLog that
     *    keeps an internal buffer structure for commands and
     *    responses.
     * 4) Initializes vlcReceiver
     * 5) Sets new gateway state
     */
      Serial1.end();
      Serial2.end();
      Serial1.flush();
      Serial2.flush();
      g_command = "";
      vlcReceiver.startListening();
      gatewayState = LISTENING;
    break;
    
    case LISTENING:
    /**
     * The Gateway will arrive here every loop cycle until a
     * packet is received once. In this state keeps uploading
     * an saving vlc work.
     * 1) vlcReceiver workOnLoop()
     * 2) Checks is there is packets for reading
     *  CASE YES: 
     *    2.1): Stops listening for packets.
     *    2.2): Reads packet an store it in the command variable
     *    2.1): Checks id within HistoryLog. Checks if the packet
     *          has been emitted with the last same id.
     *      CASE YES:
     *        2.1.1): Sets id and response variables with HistoryLog
     *        2.1.2): Sets new state to COM_LAMP
     *      CASE NO:
     *        2.1.3): Updates HistoryLog with the id an empty
     *                response
     *        2.1.4): Sets new state to COM_ROBOT
     *        
     * 3) Clear internal variables:
     *    command
     *    response
     *    Except for HistoryLog that
     *    keeps an internal buffer structure for commands and
     *    responses.
     * 4) Initializes vlcReceiver
     * 5) Sets new gateway state
     */
      vlcReceiver.workOnLoop();
      if(vlcReceiver.available()){
        vlcReceiver.stopListening();
        char currentChar = '0';
        // Receive Command
        g_command = "";
        while(vlcReceiver.read(&currentChar)){
          g_command.concat(currentChar);
        }
        // Get ID
        char id;
        id = g_command.charAt(1);
        g_id = id & 0x03;
        
        if(g_id != historyLog._id){
          g_command.concat('\n');
          g_command.remove(0,2);
          gatewayState = COM_ROBOT;
        }else{
          gatewayState = COM_LAMP;
        }
      }
    break;
    case COM_ROBOT: /**
     * The Gateway will be here only once until the packet has
     * been transmitted.
     *  1): Enables Robot Serial
     *  2): Sends the command to Robot Serial
     *  3): Waits for response with timeout
     *  4): Check response length and first bit:
     *      CASE response length equal 0: A timeout has happened
     *        Nothing has been sent.
     *      CASE response length equal 1 and first bit is equal ACK or ERR:
     *        An error or acknowledge has been received
     *            4.1): Updates HistoryLog with the response obtained
     *            4.2): Disables Robot Serial
     *            4.3): Enables OCC lamp Serial
     *            4.4): Sends the response over OCC lamp Serial.
     *            4.5): Disables OCC lamp Serial
     *      CASE response length greater than 1 and first bit is equal DATA:
     *        A data message has been received.
     *            4.6): Updates HistoryLog with the response obtained
     *            4.7): Disables RobotSerial.
     *            4.8): Enables OCC lamp Serial.
     *            4.9): Sends the response over OCC lamp Serial.
     *            4.10): Disables OCC lamp Serial.
      */
      Serial1.begin(9600);
      Serial1.print(g_command);
//      Serial.print(g_command); //TODO Debug
      uint8_t response[15];
      uint8_t lengthResponse;
      lengthResponse = Serial1.readBytesUntil((uint8_t)COM_END, response, 100);
      if(lengthResponse==0){
          gatewayState = INIT;
      }else if(lengthResponse == 1){
        if(response[0] == COM_ACK){
            g_response[0] = COM_ACK;
            g_response[1] = g_id+COM_MAGIC; //IMPORTANT: Necesity of shift id for encoded data
            g_response[2] = COM_END;
            g_responseSize = 3;
            gatewayState = COM_LAMP;
        }else if(response[0] == COM_ERR){
            g_response[0] = COM_ERR;
            g_response[1] = g_id+COM_MAGIC;//IMPORTANT: Necesity of shift id for encoded data
            g_response[2] = COM_END;
            g_responseSize = 3;
            gatewayState = COM_LAMP;
        }
      }else if(lengthResponse > 1 && response[0] == COM_DATA){
//        Data has been sent
            g_response[0] = COM_DATA;
            g_response[1] = g_id+COM_MAGIC;//IMPORTANT: Necesity of shift id for encoded data
            
            for(int i = 1; i<lengthResponse; i++){
              g_response[i+1] = response[i];
            }
            g_response[lengthResponse+1] = COM_END;
            g_responseSize = lengthResponse+2;
            gatewayState = COM_LAMP;
      }else{
//        An error has happened on communication.
          Serial.print("ERROR ON COMUNICATION> ");
          gatewayState = INIT;
      }
      //ACK
      Serial1.end();
      
    break;
    case COM_LAMP: 
    /**
     * The Gateway will be here only once until the packet has
     * been transmitted.
     *        1): Enables OCC lamp Serial.
     *        2): Sends the saved response over OCC lamp Serial
     *        3): Disables OCC lamp Serial.
      */
//      Serial.println("STATE: COM_LAMP");

      Serial2.begin(9600);
      Serial2.write(g_response,g_responseSize);
      Serial2.end();
      Serial.print("Response sent to Lamp> ");
      Serial.println(g_response[0]);
      Serial.print("Response ID sent to Lamp> ");
      Serial.println(g_response[1]);
      Serial.print("ResponseSize> ");
      Serial.println(g_responseSize);
      Serial.println("DATA=========>");
      for(int i = 0; i<g_responseSize;i++){
        Serial.println(g_response[i]);
      }
      gatewayState = INIT;
    break;
    }
}
