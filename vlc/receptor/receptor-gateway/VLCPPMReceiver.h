/*
   VLCPPMReceiver.h
   Software vlc library for Arduino using PPM modulation.
   Caracteristics:
   -> Interrupt-driven receiver
   -> Interrupt-driven receiver
   -> ATmega 2560 support
*/
#include "RingBuffer.hpp"
#ifndef VLCPPMReceiver_h
#define VLCPPMReceiver_h

/******************************************************************************************
    DEFINITIONS
******************************************************************************************/

#define PIN_ISCALIBRATED_INDICATOR 50
#define VLC_RECEIVER_PIN 2
#define VLC_CHIP_FREQUENCY 40000 //In Hertzs

#define VLC_HEADCHIPS 10
#define VLC_TAILCHIPS 11
#define VLC_ERRORCHIPS 15
#define VLC_IDLECHIPS 5

/*****************************************************************************************
   Class definitions
*****************************************************************************************/

class VLCPPMReceiver {

  private:

    /**
     * Machine states:
     * NONSET -> Non used, but It's set just in case it is needed in future releases.
     * INIT -> Initial states. The machine has this state after creation.
     * LISTENING -> Listening for incomming packets. In this state, the receiver is after
     *              headers, discarting tails and errors.
     * RECEIVING -> Receiving an incomming packet.
     * ONERROR -> Transition state after an error is detected.
     * WAITINGFOR_DEMODULATION -> The packet is correctly saved in buffer.
     * DEMODULATING -> The packet is being procesed.
     */
    enum RECEIVER_STATE {
      NONSET,
      INIT,
      LISTENING,
      RECEIVING,
      ONERROR,
      WAITINGFOR_DEMODULATION,
      DEMODULATING,
    };

    RECEIVER_STATE rx_state = NONSET;

    /*****************************************************************************************
      Data variables
    *****************************************************************************************/
    volatile RingBuffer<uint16_t, 255> delaysBuffer;
    volatile RingBuffer<char, 255> charBuffer;

    /*****************************************************************************************
      Pin variables & functions
    *****************************************************************************************/

    void setReceiverPin(uint8_t receiverPin);
    void initIndicatorPins(uint8_t isCalibratedIndicatorPin);


    /*****************************************************************************************
      Timer variables and functions
    *****************************************************************************************/

    volatile uint16_t headerCount;
    volatile uint16_t tailCount;
    volatile uint16_t errorCount;

    volatile uint16_t idleCount;

    /*
       chipCount: number of counter ticks used to characterize the chip time
    */
    volatile uint16_t chipCount;

    /*
       errorCountCorrection: number of counter ticks used to characterize
       the estimated error allowed.
    */
    volatile uint16_t errorCountCorrection;
    volatile float errorPercentage;

    bool configureTimerFor(float chipFrecuency);
    void computeTimingCounts(uint16_t chipCount);

    /*****************************************************************************************
      Demodulation variables & functions
    *****************************************************************************************/

    /*
       isInverted: the ppm is inverted. The position of signal is on low logic level.
    */
    volatile uint8_t isInverted;
    volatile uint8_t isDifferencial;
    volatile uint8_t isAvailableForDemodulation;
    volatile uint16_t delayTime;
    volatile uint16_t oldTime;

    volatile uint16_t debugCount = 0;
    uint8_t demodulateData();
    uint16_t getTimeDelay();
    uint8_t getBitsForDelay(uint16_t delayTime, uint8_t *pointerBits);

    /*****************************************************************************************
      State variables & functions
    *****************************************************************************************/

    /*
       @var: isReceiving -> True: if it's is receiving a packet. The
                            HEADER frame was deteted and now the receiver
                            can be processing the packet id, or the packet
                            lenght

                            False: if it's not receiving a packet. The HEADER
                            frame must be deteted prior to be receiving. The
                            receiver enters in a false state for isReceiving
                            when it's reads all the bytes comming.
    */
    volatile uint8_t isReceiving;
    volatile uint8_t isAvailable;
    volatile uint8_t isDoneReading;
    void startTimer();
    void stopTimer();


    /*****************************************************************************************
      Util variables
    *****************************************************************************************/

    //Singleton
    static VLCPPMReceiver *active_VLCPPMReceiver;

  public:

    VLCPPMReceiver(uint8_t receiverPin, float errorPercentageOnSymbol, uint8_t isInverted);
    ~VLCPPMReceiver();

    /*****************************************************************************************
      Reception variables & functions
    *****************************************************************************************/
    boolean available();
    uint8_t read(uint8_t *byteData);
    inline void recv() __attribute__((__always_inline__));
    void setIndicatorCalibrated(bool isCalibrated);

    /*****************************************************************************************
      State variables & functions
    *****************************************************************************************/

    void begin(long frecuency);
    void end();
    void startListening();
    void stopListening();
    void workOnLoop();

    volatile uint8_t dHEAD;
    volatile uint8_t dTAIL;
    volatile uint8_t dERROR;

    //Static for making it easier by the interrupt handlers
    static inline void handle_interrupt() __attribute__((__always_inline__));

};

#endif /*VLCPPMreceiver_h*/
