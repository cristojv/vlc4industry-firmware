/**
 * \file RingBuffer.hpp
 * \version 1.1.1
 * \brief Generic ring buffer implementation for embedded targets
 * \author Cristo Manuel Jurado Verdu
 * \company IDeTIC
 * 
 * \characteristics: 1.1 C++11 standard and above
 *                   1.2 No excpetions, RTTI, virtual functions and dynamic memory allocation
 *                   1.3 No wasted slots
 *                   1.4 Lock-free atomic operation in SPSC cases
 *                   1.5 Underrun and overrun checks in insert/remove functions
 *                   1.6 High efficient on most microcontroller architectures
*/

#include <stdint.h>
#ifndef RING_BUFFER_H

#define RING_BUFFER_H
#define RING_BUFFER_OK 1
#define RING_BUFFER_FAIL 0

template<typename type = uint8_t, uint8_t bufferSize = 100> class RingBuffer {
  
  public:
    
    /*!
     * \brief Intentionally empty constructor - nothing to allocate
     * \warning If class is instantiated on stack, heap or inside noinit section
     * then the buffer has to be explicitly cleared before use
     */
   RingBuffer(){
     head = tail = &ringBuffer[0];
     count = 0;
   };

    /*!
     * \brief Intentionally emtpy destructor - nothing has to be released
     */
    ~RingBuffer(){
    };

    /*!
     * \brief Push data into the buffer array. FIFO model
     */
    int push(type data){
      type volatile *nextTail = tail + 1;         //Points to the next memory address
      
      if(nextTail == &ringBuffer[bufferSize]){
        nextTail = &ringBuffer[0];             //Envelops pointer to point to the start memory address
      }
    
      if(isFull()){                            //Checks if buffer is Full
        return (RING_BUFFER_FAIL);
      }else{
        *tail = data;                         //Adds value
        tail = nextTail;                       //Change pointer's target
        count ++;                              //Increments the count
        return (RING_BUFFER_OK); //Done
      }
    };

    /*!
     * \brief Pop first data from the buffer array. FIFO model
     */
    int pop(type *dataPointer){
      type volatile *nextHead = head + 1;         //Points to the next memory address
      
      if(nextHead == &ringBuffer[bufferSize]){
        nextHead = &ringBuffer[0];             //Envelops pointer to point to the start memory address
      }
      
      if(isEmpty()){                           //Checks if buffer is Empty
        return (RING_BUFFER_FAIL);
      }else{
        *dataPointer = *head;
        //Atomic operation
        head = nextHead;
        count --;
        return (RING_BUFFER_OK); //Done
      }
     };

    /*!
     * \brief Gets number of insterted elements
     */
    uint8_t getCount() const{
      return (count); //Done
    };
    /*!
     * \brief Checks if buffer is empty
     */
     bool isEmpty(void) const{
      return (tail == head) && (count==0);
     };

     /*!
      * \bierf Checks if buffer is full
      */
     bool isFull(void) const{
      return (tail == head) && (count==bufferSize);
     };
  private:
    type volatile *head; // Head index
    type volatile *tail; // Tail index
    type ringBuffer[bufferSize];
    uint8_t count;
};
#endif /*__RING_BUFFER_*/



