/**
 * Encoder.h - Encoder library
 */

/**
 * DEFINITIONS
 */

#ifndef ENCODER_h
#define ENCODER_h

#define COM_ERR 0
#define COM_ACK 1
#define COM_DATA 2
#define COM_END 3
#define COM_FLUSH 4
#define COM_MAGIC 5

#include "stdint.h"
/**
 * CLASS PROTOTYPE
 */

class Encoder {
public:

  int encodeData(uint8_t *rawBuffer, int rawSize, uint8_t *encodedBuffer, int encodedSize);
  int decodeData(uint8_t *encodedBuffer, int encodedSize, uint8_t *decodedBuffer, int decodedSize);

};

#endif /*__RING_BUFFER_*/
