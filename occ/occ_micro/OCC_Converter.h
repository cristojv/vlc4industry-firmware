/**
 * \file OCC_Converter.h
 * \version 1.1.1
 * \brief OCC converter integrates an OOK modulator from standard ASCII incomming data.
 * \author Cristo Manuel Jurado Verdu
 * \company IDeTIC
*/

#ifndef OCC_Converter_H
#define OCC_Converter_H

#include "avr/pgmspace.h"
#include "stdlib.h"
#include "RingBuffer.hpp"
#include "IterableBuffer.hpp"

/**
 * Definitions
 */
#define MAX_IN_BUFFER_SIZE 100
#define MAX_OUT_BUFFER_SIZE 6
#define MAX_CURRENT_ID 4

enum ConverterState{CS_Init, CS_DataTx, CS_IdleTx};

class Converter{
  
  public:

    Converter(uint8_t maxRepetitions);
    ~Converter();
    uint8_t loadNextState();
    uint8_t getNextState() const;
    bool insertData(uint8_t data);
    
  private:
    bool _isDataWaiting;
    bool convertData(uint8_t data);
    uint8_t _maxRepetitions;
    uint8_t _countRepetitions;
    uint8_t _idleSignal;
    uint8_t _outSignal;
    enum ConverterState _state;
    uint8_t _currentID;
    RingBuffer<uint8_t,MAX_IN_BUFFER_SIZE> _inBuffer;
    IterableBuffer<uint8_t,MAX_OUT_BUFFER_SIZE> _outBuffer;
};
#endif

