/**
 * \file OCC_Converter.cpp
 * \version 1.1.1
 * \brief OCC converter integrates an OOK modulator from standard ASCII incomming data.
 * \author Cristo Manuel Jurado Verdu
 * \company IDeTIC
*/

/**
 */
#include "OCC_Converter.h"
#include "Arduino.h"
Converter::Converter(uint8_t maxRepetitions){
  _maxRepetitions = maxRepetitions;
  _countRepetitions = 0;
  _idleSignal = 0x11;
  _outSignal = _idleSignal;
  _state = CS_IdleTx;
  _isDataWaiting = false;
  _currentID = 0;
}

Converter::~Converter(){}

uint8_t Converter::loadNextState(){
  
  /**!
   * Transist between diferent states:
   * CS_Init -> CS_IdleTx
   * CS_IdleTx -> CS_DataTx
   * CS_DataTx -> CS_IdleTx
   */
   
  switch(_state){
    case CS_IdleTx:
      /**!
       * Transist to CS_Data if: 
       * 1) isDataWaiting for being sent
       * 2) current idleSignal has finalized being transmitted
       */
       
      if(_isDataWaiting && !(_outSignal & 0x1)){//OJO!
        _outBuffer.reset();
        _state = CS_DataTx;
      }
    break;
    
    case CS_DataTx:
      /**!
       * Transist to CS_Idle if:
       * 1) There is no data to be sent in outBuffer
       * 2) There is no data to be sent it in inBuffer
       */
       
      if(_outBuffer.isEmpty() && _inBuffer.isEmpty()){
        _outSignal = (_idleSignal >> 1) | (_idleSignal << 7);
        _isDataWaiting = false;
        _state = CS_IdleTx;
      }
      break;
  }
  
  /**!
   * Execute State functions:
   * CS_Init -> No applicable. This state goes directly into CS_IdleTx
   * CS_IdleTx -> Send Idle Signal. Green - Red - Blue - None swith
   * CS_DataTx -> Send Data Signal.
   */
   
  switch(_state){
    
    case CS_IdleTx:
      /**
       * Circular shift of the _outSignal.
       */
      _outSignal = (_outSignal << 1) | (_outSignal >> 7);
      return true;
    break;
    
    case CS_DataTx:
    
      if(!_outBuffer.isEmpty()){
        uint8_t data = 0;
        bool result = _outBuffer.readNext(&data);
        _outSignal = data;
        
        if(_outBuffer.hasBeenRead()){
          _countRepetitions++;
          _outBuffer.resetRead();
          if(_countRepetitions >_maxRepetitions){
            _countRepetitions = 0;
            _outBuffer.reset();
          }
        }
        return true;
      }else{
        if(!(_inBuffer.isEmpty())){
          uint8_t data = 0;
          if(_inBuffer.pop(&data)){
            if(convertData(data)){
              _countRepetitions = 0;
              uint8_t value = 0;
              bool result = _outBuffer.readNext(&value);
              _outSignal = value;
//              Serial.println(data);
//              Serial.println(value);
              return true;
            }
          }
        }else{
          _outBuffer.reset();
        }
      }
      return false;
      break;
  }
}

uint8_t Converter::getNextState()const{
  return _outSignal;
}

bool Converter::insertData(uint8_t data){
  bool result = _inBuffer.push(data);
  _isDataWaiting = true;
  return result;
}

/**!
 * \brief Converts Byte data into a three channel OOK modulated signal.
 * 
 * If the bits of the byte are named as: a-b-c-d-e-f-g-h (8 bits) then
 * the signal is composed by:
 * Green channel ->0-1-1-0-1-0 (Synchronization signal)
 * Red channel -> 0-x0-b-d-f-h
 * Blue channel -> 0-x1-a-c-e-g
 * 
 * Where x1 and x0 are the id of the packet.
 * NOTE: There is a guard interval where all the pins are set to zero,
 * to prevent packet collision.
 * 
 * Example (a in ASCII - 97 in DEC - 01100001 in BIN) 
 * Green channel ->   1-1-0-1-0
 * Red channel ->     1-1-0-0-1
 * Blue channel ->    0-0-1-0-0
 * 
 * The signal output every tick is a combination of the Green,Red and Blue channel.
 * Thus creating a 5 byte array:
 * a[0] = (Bit 1 Blue, Bit 1 Red, Bit 1 Green)
 * a[0] = 0 - 0 - 1
 * 
 * Following the last example:
 * a[0] = B00000-011
 * a[1] = B00000-011
 * a[2] = B00000-100
 * a[3] = B00000-001
 * a[4] = B00000-010
 */

bool Converter::convertData(uint8_t data){
//  Serial.print(data,HEX);
  uint8_t id = _currentID;
  _outBuffer.reset();
  uint8_t nextByte = ((id&0x03)<<1) | (0x1);
  if(_outBuffer.insert(0x00)){
    if(_outBuffer.insert(nextByte<<5)){
      nextByte = (data&(0x03<<6))>>5 | (0x1);
      
      if(_outBuffer.insert(nextByte<<5)){
        nextByte = (data&(0x03<<4))>>3 | (0x0);
        
        if(_outBuffer.insert(nextByte<<5)){
          nextByte = (data&(0x03<<2))>>1 | (0x1);
          
          if(_outBuffer.insert(nextByte<<5)){
            nextByte = (data&(0x03<<0))<<1 | (0x0);
            
            if(_outBuffer.insert(nextByte<<5)){
              _currentID = (_currentID+1)%MAX_CURRENT_ID;
              return true;
            }
          }
        }
      }
    }
  }
  return false;
}
