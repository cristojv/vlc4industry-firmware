/**
   \file OCC_main.ino
   \date 20172002.1830
   \author Cristo Manuel Jurado Verdu
   \company IDeTIC
   \brief Main program for OCC transmitter

   \mainCharacteristics
    1) Interrupt-driver transmission
    2) Support for Arduino ATmega 2560
    3) Support for 8 signal channels, designed for 3
      3.1) Channel 1 (Green). Frame Synchrozation signal. PIN 22
      3.2) Channel 2 (Red). Less significant Data signal. PIN 23
      3.3) Channel 3 (Blue). Most significant Data signal. PIN 24
*/
#include "Arduino.h"
#include "avr/pgmspace.h"
#include "OCC_Converter.h"
#include "RingBuffer.hpp"
#include "Encoder.h"

#define COM_ERR 0
#define COM_ACK 1
#define COM_DATA 2
#define COM_END 3
#define COM_FLUSH 4
#define COM_MAGIC 5

//Definitions

/**!
   DEBUG options:
   0 - No print debug
   1 - Print basic debug options. Configuration.
   2 - Print advanced debug options. Signal.
*/
#define DEBUG 1 //Prints Serial debug

/**
   Pin port 1 - from digital pin 22 to digital pin 29.
   Up to 8 independent channels.
   pinMask 0000-0001 -> pin 22
   pinMask 0000-0010 -> pin 23
   pinMask 0000-0100 -> pin 24
*/

#define pinPort 1

/**
   GLOBAL VARIABLES,:
   ->ISR Flags:
      -> isPinStateUpdated. Reveals if the pin state was update via port register.
      -> isSerialCompleted.
   ->ISR Data:
      -> pinNextState. This pin state is going to be update vie timer interruption onto the port register.
      -> pinPortRegister(Pointer): pointer to the port register.

   ->Converter. Note: Converter is not needed to be in a global scope, but it's neccesary cause
                      arduino setup(), loop() model.
*/
//uint8_t randomb[] = {195, 76, 97, 200, 133, 35, 93, 220, 209, 113, 223, 186, 204, 213, 225, 120, 217, 150, 74, 143, 84, 135, 193, 12, 162, 248, 182, 32, 86, 90, 137, 219, 138, 230, 222, 110, 234, 203, 23, 187, 210, 125, 56, 58, 155, 63, 96, 246, 89, 142, 159, 252, 233, 158, 237, 191, 7, 126, 40, 80, 104, 242, 244, 253, 235, 13, 17, 51, 60, 3, 228, 4, 146, 9, 105, 241, 140, 39, 10, 59, 177, 92, 131, 31, 46, 251, 226, 163, 170, 106, 178, 70, 109, 129, 175, 100, 28, 247, 201, 50, 17, 248, 43, 191, 20, 22, 4, 107, 139, 65, 64, 112, 69, 190, 82, 232, 50, 160, 118, 155, 42, 229, 31, 98, 165, 11, 146, 51, 36, 167, 201, 79, 46, 35, 251, 91, 178, 13, 133, 70, 238, 244, 224, 207, 8, 154, 32, 105, 157, 44, 10, 252, 138, 68, 97, 15, 200, 120, 184, 16, 113, 55, 216, 1, 163, 198, 19, 128, 85, 126, 92, 39, 142, 103, 179, 94, 145, 141, 5, 99, 63, 62, 122, 247, 195, 183, 49, 151, 150, 254, 235, 124, 168, 136, 74, 197, 95, 243, 80, 67, 106, 240, 41, 124, 89, 26, 125, 182, 229, 249, 28, 138, 143, 218, 194, 113, 227, 181, 238, 47, 203, 228, 231, 217, 8, 211, 236, 207, 103, 38, 216, 233, 5, 235, 190, 199, 57, 150, 202, 205, 173, 15, 253, 220, 226, 139, 65, 209, 123, 183, 252, 75, 184, 170, 196, 121, 189, 68, 40, 187, 145, 160, 80, 52, 85, 53, 114, 221, 93, 16, 127, 7, 77, 232, 74, 166, 50, 49, 224, 140, 185, 6, 152, 62, 219, 254, 250, 29, 67, 34, 215, 206, 81, 180, 116, 100, 36, 132, 101, 201, 116, 38, 64, 156, 87, 100, 223, 72, 60, 43, 211, 178, 30, 253, 160, 79, 65, 10, 139, 254, 162, 95, 31, 71, 86, 23, 69, 55, 150, 57, 70, 37, 241, 133, 212, 58, 48, 240, 193, 21, 250, 234, 51, 114, 172, 232, 170, 147, 67, 197, 68, 8, 173, 204, 32, 205, 228, 99, 246, 88, 171, 78, 164, 195, 194, 200, 97, 185, 96, 180, 142, 230, 183, 159, 6, 243, 167, 208, 101, 61, 138, 93, 141, 238, 74, 186, 226, 4, 184, 35, 73, 143, 9, 215, 7, 40, 5, 252, 245, 14, 221, 66, 241, 7, 164, 178, 106, 22, 31, 199, 128, 206, 136, 15, 137, 194, 250, 216, 75, 50, 100, 74, 248, 39, 115, 43, 23, 186, 54, 184, 35, 81, 116, 57, 156, 190, 108, 201, 24, 224, 12, 37, 246, 102, 69, 78, 236, 53, 111, 6, 19, 72, 89, 217, 86, 141, 3, 67, 32, 168, 252, 239, 58, 70, 225, 38, 204, 105, 254, 64, 193, 175, 158, 103, 242, 139, 127, 28, 208, 87, 76, 99, 145, 161, 104, 214, 29, 166, 187, 20, 143, 149, 14, 97, 130, 152, 124, 45, 169, 165, 191, 20, 242, 21, 78, 70, 30, 241, 218, 188, 118, 233, 80, 193, 214, 66, 96, 81, 8, 134, 113, 147, 157, 65, 91, 164, 153, 29, 90, 45, 82, 174, 129, 249, 248, 139, 56, 107, 98, 125, 16, 28, 213, 114, 219, 135, 67, 112, 224, 172, 148, 9, 221, 104, 150, 102, 231, 154, 121, 167, 200, 169, 210, 226, 10, 42, 68, 225, 75, 238, 116, 132, 61, 253, 254, 144, 41, 180, 89, 1, 40, 234, 137, 74, 247, 192, 86, 140, 131, 12, 15, 83, 3, 31, 120, 94, 168, 206, 245, 55, 233, 74, 7, 213, 65, 164, 198, 244, 97, 33, 238, 140, 75, 210, 237, 217, 12, 195, 144, 103, 17, 92, 25, 56, 34, 59, 67, 153, 32, 234, 123, 83, 176, 154, 197, 253, 57, 61, 247, 62, 202, 93, 193, 207, 15, 63, 27, 82, 252, 0, 84, 53, 13, 102, 50, 4, 250, 152, 219, 150, 246, 45, 6, 68, 100, 111, 94, 80, 146, 190, 227, 35, 137, 22, 101, 77, 170, 162, 145, 8, 218, 139, 99, 54, 96, 158, 129, 49, 119, 113, 149, 243, 91, 114, 175, 225, 224, 70, 181, 98, 149, 233, 175, 204, 159, 253, 31, 119, 200, 61, 66, 114, 109, 240, 12, 121, 68, 57, 100, 249, 70, 222, 208, 99, 117, 137, 214, 62, 124, 55, 136, 169, 3, 142, 6, 75, 181, 96, 104, 73, 174, 40, 28, 44, 146, 210, 46, 225, 64, 246, 154, 29, 85, 231, 74, 235, 97, 188, 158, 161, 4, 238, 103, 10, 82, 13, 230, 102, 76, 213, 217, 92, 122, 39, 83, 43, 125, 107, 33, 78, 30, 237, 141, 197, 186, 19, 80, 218, 0, 135, 243, 77, 128, 176, 127, 241, 168, 22, 95, 192, 141, 58, 4, 76, 214, 182, 48, 192, 176, 98, 9, 71, 104, 69, 50, 198, 52, 146, 217, 103, 167, 86, 168, 212, 0, 213, 177, 223, 95, 24, 88, 143, 164, 87, 135, 254, 32, 92, 152, 62, 121, 27, 184, 132, 114, 169, 237, 234, 80, 17, 216, 22, 82, 39, 25, 66, 100, 74, 11, 145, 240, 218, 156, 188, 148, 31, 153, 19, 151, 230, 147, 99, 49, 91, 63, 126, 231, 55, 124, 96, 112, 242, 131, 140, 166, 200, 228, 171, 170, 173, 196, 73, 28, 38, 34, 203, 36, 149, 138, 160, 4, 15, 175, 208, 114, 204, 244, 239, 88, 190, 248, 55, 17, 70, 178, 237, 202, 235, 163, 231, 93, 53, 92, 44, 227, 125, 104, 187, 143, 245, 222, 142, 141, 58, 23, 27, 241, 203, 147, 89, 138, 98, 149, 156, 221, 29, 228, 43, 188, 144, 14, 18, 253, 37, 31, 13, 95, 173, 211, 207, 130, 250, 133, 50, 25, 1, 209, 51, 103, 24, 72, 74, 52, 254, 121, 100, 124, 226, 84, 66, 75, 63, 97, 129, 236, 183, 0, 42, 251, 238, 249, 3, 160, 240, 60, 140, 242, 108, 109, 232};
volatile uint8_t g_isPinStateUpdated;
volatile uint8_t g_isSerialCompleted;
volatile uint8_t g_pinNextState;
volatile uint8_t *g_pinPortRegister;
Converter converter(60);
Encoder encoder;
/**
   Global PIN Functions:
   -> configureChannelPins: configure the operation mode for signal pins
                            MODE: Output, initial state (LOW).
   -> flushNext:            updates pin port. Pins updates their real state.
*/

void configurePins_init() {
  //  g_pinPortRegister = portOutputRegister(pinPort);

  for (int i = 0; i < 8; i++) {
    pinMode(i, OUTPUT);
  }
  pinMode(A0, OUTPUT);
  pinMode(A1, OUTPUT);
  pinMode(A2, OUTPUT);
  pinMode(1, INPUT);
  pinMode(0, INPUT);
  digitalWrite(A0, LOW);
  digitalWrite(A1, LOW);
  digitalWrite(A2, LOW);
  digitalWrite(1, HIGH);
  digitalWrite(0, HIGH);
  pinMode(A3, INPUT);
}

void flushNext(uint8_t pinsState) {
  //  PORTD = pinsState;
  PORTF = pinsState;
  //  *g_pinPortRegister = pinsState;
}

/**
   TIMER Functions:
   -> configureTimerInterruptionFor:  configure timer to launch a interruption every
                                      chip's time.
   -> startTimer:                     enable timer interruption
   -> stopTimer:                      disable timer interruption
*/

bool configureTimerInterruptionFor(float chipFrecuency) {
  /**!
     ConfigureTimer routine
     1) Disable interrupts
     2) Clear timer variables.
     3) Choose minimum prescaling factor
     4) Check clock counter selection before updating registers.
     5) Configure. (Set registers):
        -> CTC mode [Clear Timer on Compare Match]
        -> Prescaler
        -> Clock counter.
     6) Enable interrupts.
  */
  noInterrupts();
  TCCR3A = 0;
  TCCR3B = 0;
  TCNT3 = 0;
  int prescaler = 8;
  float chipCountFloat = 0.0;

  /**
     Prescaler Bit Register:
     Bits 8 -> Prescaler 2
     Bits 64 -> Prescaler 3
     Bits 256 -> Prescaler 4
     Bits 1024 -> Prescaler 5
  */
  uint8_t prescalerMask;

  for (prescalerMask = 2; prescalerMask <= 5; prescalerMask++) {
    /* Must be taken Arduino definition F_CPU = 16MHz*/
    chipCountFloat = F_CPU / (chipFrecuency * prescaler);

    /* Timer 3 has 16 bit resolution so it's neccesary to check the correct clock counter value.
       chipCount must be less than 2^16 = 65536*/
    if (chipCountFloat < 65536) {
      if (prescalerMask >= 5) {
        return false;
      } else {
        break;
      }
    } else {
      prescaler = prescaler * 8;
    }
  }

  TCCR3B |= (1 << WGM32);
  TCCR3B |= prescalerMask;

  /*
     Fine grain calibration.
     This step substract the average time of entering and exiting
     the timer interrupt routine.
     The average number of cicles is 19.
  */

  double interruptDelay = 1 / F_CPU * 19 / 2.0;
  float countCorrection = interruptDelay * float(F_CPU) / float(prescaler) / 2.0;
  uint16_t chipCount = round(chipCountFloat - countCorrection);

  OCR3A = chipCount;
  interrupts();


#if DEBUG
  Serial.print("ChipCount - > ");
  Serial.print(chipCountFloat);
  Serial.print(" : ");
  Serial.println(chipCount);
  Serial.print(countCorrection);
#endif

  return true;
}

void startTimer() {
  /**!
     1) Disable interrupts
     2) Enable Compare Match Interruption of Timer 3
     3) Enable interrupts.
  */
  noInterrupts();
  TIMSK3 |= (1 << OCIE3A);
  interrupts();
}

void stopTimer() {
  /**!
     1) Disable interrupts
     2) Disable Compare Match Interruption of Timer 3
     3) Enable interrupts
  */
  noInterrupts();
  TIMSK3 &= ~(1 << OCIE3A);
  interrupts();
}

void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);
  //   while (!Serial) ;
  /**!
     Initialization of variables.
     1) Global variables (with prefix g_)
     2) Local variables for configuration

     Configuration.
     1) Enable Serial with 115200 baudrate
     2) Configure Pins
     3) Configure Timer Interruption.
     4) Set TCCR0B and TCCR0A to zero. Note: This is an important
        step to reduce the jitter effect of the output signal.
  */

  g_isPinStateUpdated = true;
  g_isSerialCompleted = true;
  g_pinNextState = 0;

  //  float frecuency = 960 * 2 * 2;
  float frecuency = 1800 * 2;

#if DEBUG==1
  Serial.println("OCC Transmitter");
  Serial.print("Frecuency: ");
  Serial.println(frecuency);
#endif

  configurePins_init();
  configureTimerInterruptionFor(frecuency);

  /*Don't remove*/
  TCCR0B = 0;
  TCCR0A = 0;
  /*End: Don't remove*/

  startTimer();
}

uint8_t g_isProcessing = 0;

RingBuffer<uint8_t, 100> g_outBuffer;

void loop() {

  /**!
     Colaboration architecture
     1) Checks interrupt flag. If the pin state has been flushed, then update next Pin State.
     2) Checks serial. If data is comming, push to converter.
  */

  if (g_isPinStateUpdated) {
    g_pinNextState = converter.getNextState();
    converter.loadNextState();
#if DEBUG==2
    Serial.println(g_pinNextState);
#endif
    g_isPinStateUpdated = false;
  }
  
  if (g_isProcessing == 0) {
    if (Serial1.available() > 0) {
      uint8_t inByte = (uint8_t) Serial1.read();
      if (inByte == (byte) COM_END) {
        g_isProcessing = 1;
      } else {
        uint8_t result = g_outBuffer.push(inByte);
      }
    }
  } else {
    uint8_t type;
    uint8_t id;
    if (g_outBuffer.pop(&type)) {
      if (g_outBuffer.pop(&id)) {
        id = id - COM_MAGIC; // IMPORTANT: This id if shifted on Gateway.
        if (type == (byte)COM_ACK || type == (byte)COM_ERR) {
          Serial.print("ACK or ERR> ");
          Serial.println((uint8_t)(id << 6 | type + 1 << 4));
          converter.insertData((uint8_t)(id << 6 | type + 1 << 4));
//          Flush Buffer
          uint8_t inData;
          while(g_outBuffer.pop(&inData)){
            
          }
        } else if (type == (byte)COM_DATA) {
          Serial.print("DATA> ");
          uint8_t count = g_outBuffer.getCount();
          uint8_t inData;
          uint8_t inEncodedBuffer[30];
          uint8_t inDecodedBuffer[30];
          uint8_t inEncodedBufferSize = 0;
          uint8_t inDecodedBufferSize = 0;
          while (g_outBuffer.pop(&inData)) {
            Serial.print("data ");
            Serial.print(inEncodedBufferSize);
            Serial.println(inData,DEC);
            inEncodedBuffer[inEncodedBufferSize]=inData;
            inEncodedBufferSize += 1;
          };
          inDecodedBufferSize = encoder.decodeData(inEncodedBuffer, inEncodedBufferSize, inDecodedBuffer, 30);
          Serial.print("inDecodedBufferSize> ");
          Serial.println(inDecodedBufferSize);
          Serial.println((uint8_t)(id << 6 | type + 1 << 4 | inDecodedBufferSize));
          converter.insertData((uint8_t)(id << 6 | type + 1 << 4 | inDecodedBufferSize));
          for (int i=0; i<inDecodedBufferSize; i++){
            Serial.print("data decoded");
            Serial.print(i);
            Serial.println(inData,DEC);
            converter.insertData((uint8_t)inDecodedBuffer[i]);
          }
        }
      }
    }
    g_isProcessing = 0;
  }
}
ISR(TIMER3_COMPA_vect) {
  /**
     1) Update pins.
     2) Update interrupt flag.
  */
  flushNext(g_pinNextState);
  //  PORTD = g_pinNextState;
  //  digitalWrite(6,HIGH);
  g_isPinStateUpdated = true;
}
