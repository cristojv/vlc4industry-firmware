/**
   \file occ_osa_paper.ino
   \date 20181210.0745
   \author Cristo Manuel Jurado Verdu
   \company IDeTIC
   \brief OCC transmitter, used in OSA Paper. This program was used for verification
   of the tecnology behind OCC. To use it just open the serial at 115200 bauds and
   press any key to switch between the procedure of sending 125e3 random 8 bits packets
   or the idle state.

   \mainCharacteristics
    1) Interrupt-driver transmission
    2) Support for Arduino ATmega 2560
    3) Support for 8 signal channels, designed for 3
      3.1) Channel 1 (Green). Frame Synchrozation signal. PIN 26
      3.2) Channel 2 (Red). Less significant Data signal. PIN 27
      3.3) Channel 3 (Blue). Most significant Data signal. PIN 28
*/

#include "Arduino.h"
#include "avr/pgmspace.h"
#include "occ_converter.h"

#include <stdio.h>
#include <stdlib.h>
//#include <Time.h>

//Definitions

/**!
   DEBUG options:
   0 - No print debug
   1 - Print basic debug options. Configuration.
   2 - Print advanced debug options. Signal.
*/
#define DEBUG 1 //Prints Serial debug

/**
   Pin port 1 - from digital pin 22 to digital pin 29.
   Up to 8 independent channels.
   pinMask 0000-0001 -> pin 26
   pinMask 0000-0010 -> pin 27
   pinMask 0000-0100 -> pin 28
*/

#define pinPort 1

/**
   GLOBAL VARIABLES,:
   ->ISR Flags:
      -> isPinStateUpdated. Reveals if the pin state was update via port register.
      -> isSerialCompleted.
   ->ISR Data:
      -> pinNextState. This pin state is going to be update vie timer interruption onto the port register.
      -> pinPortRegister(Pointer): pointer to the port register.

   ->Converter. Note: Converter is not needed to be in a global scope, but it's neccesary cause
                      arduino setup(), loop() model.
*/

volatile uint8_t g_isPinStateUpdated;
volatile uint8_t g_isSerialCompleted;
volatile uint8_t g_pinNextState;
volatile uint8_t *g_pinPortRegister;
Converter converter(5);

/**
   Global PIN Functions:
   -> configureChannelPins: configure the operation mode for signal pins
                            MODE: Output, initial state (LOW).
   -> flushNext:            updates pin port. Pins updates their real state.
*/

void configurePins_init() {
  g_pinPortRegister = portOutputRegister(pinPort);
  for (int i = 1; i < 52; i++) {
    pinMode(i, OUTPUT);
    digitalWrite(i, LOW);
  }
}

void flushNext(uint8_t pinsState) {
  *g_pinPortRegister = pinsState << 4;
}

/**
   TIMER Functions:
   -> configureTimerInterruptionFor:  configure timer to launch a interruption every
                                      chip's time.
   -> startTimer:                     enable timer interruption
   -> stopTimer:                      disable timer interruption
*/

bool configureTimerInterruptionFor(float chipFrecuency) {
  /**!
     ConfigureTimer routine
     1) Disable interrupts
     2) Clear timer variables.
     3) Choose minimum prescaling factor
     4) Check clock counter selection before updating registers.
     5) Configure. (Set registers):
        -> CTC mode [Clear Timer on Compare Match]
        -> Prescaler
        -> Clock counter.
     6) Enable interrupts.
  */
  noInterrupts();
  TCCR3A = 0;
  TCCR3B = 0;
  TCNT3 = 0;
  int prescaler = 1;
  float chipCountFloat = 0.0;

  /**
     Prescaler Bit Register:
     Bits 8 -> Prescaler 2
     Bits 64 -> Prescaler 3
     Bits 256 -> Prescaler 4
     Bits 1024 -> Prescaler 5
  */
  uint8_t prescalerMask;

  for (prescalerMask = 1; prescalerMask <= 5; prescalerMask++) {
    /* Must be taken Arduino definition F_CPU = 16MHz*/
    chipCountFloat = (F_CPU / ((chipFrecuency * prescaler))) - 1;

#if DEBUG
    Serial.print(chipCountFloat);
#endif
    /* Timer 3 has 16 bit resolution so it's neccesary to check the correct clock counter value.
       chipCount must be less than 2^16 = 65536*/
    if (chipCountFloat < 65536) {
      if (prescalerMask >= 5) {
        return false;
      } else {
        break;
      }
    } else {
      prescaler = prescaler * 8;
    }
  }

  TCCR3B |= (1 << WGM32);
  TCCR3B |= prescalerMask;

  /*
     Fine grain calibration.
     This step substract the average time of entering and exiting
     the timer interrupt routine.
     The average number of cicles is 19.
  */

  double interruptDelay = 1 / F_CPU * 19 / 2.0;
  float countCorrection = interruptDelay * float(F_CPU) / float(prescaler) / 2.0;
  uint16_t chipCount = round(chipCountFloat - countCorrection);

  OCR3A = chipCount;
  interrupts();

  converter.setMaxNumberOfRepetitions(round(chipFrecuency/30.0*2/5.0+1));

#if DEBUG
  Serial.println("Configuration");
  Serial.print("Choosen prescaler: ");
  Serial.println(prescaler);
  Serial.print("Minimum time for prescaler: ");
  Serial.print(F_CPU);
  Serial.print(", ");
  Serial.print(float(float(prescaler * 1000000) / float(F_CPU)));
  Serial.println(" in us: ");
  Serial.print("Clock prescaler: ");
  Serial.println(CLKPR, HEX);
  Serial.print("ChipCount - > ");
  Serial.print(chipCountFloat);
  Serial.print(" : ");
  Serial.println(chipCount);
  Serial.println(countCorrection);
  Serial.print("MaxNumberOfRepetitions: ");
  Serial.print(round(chipFrecuency/30.0*2/5.0+1));
#endif

  return true;
}

void startTimer() {
  /**!
     1) Disable interrupts
     2) Enable Compare Match Interruption of Timer 3
     3) Enable interrupts.
  */
  noInterrupts();
  TIMSK3 |= (1 << OCIE3A);
  interrupts();
}

void stopTimer() {
  /**!
     1) Disable interrupts
     2) Disable Compare Match Interruption of Timer 3
     3) Enable interrupts
  */
  noInterrupts();
  TIMSK3 &= ~(1 << OCIE3A);
  interrupts();
}

// Basic State Machine of just one state.
#define MAX_NUMBER_OF_PACKETS 50

uint8_t g_sendContiniously = 0;
uint16_t g_numberOfPacketsSent = 0;

void changeState() {
  g_sendContiniously ^= 1;
}

//time_t t;

uint8_t generateRandomData() {
  uint8_t data = random(127);
  return data;
}
//uint8_t dataV[12]={'H','e','l','l','o',' ','w','o','r','l','d','!'};
//uint8_t indexV = 0;
//uint8_t generateRandomData(){
//  uint8_t data = dataV[indexV];
//  indexV = (indexV+1)%12;
//  return data;
//
//}

typedef enum {
  OCC_INIT,
  OCC_TX,
  OCC_STOP,
  OCC_CONFIG
} OCCState;

OCCState occState = OCC_INIT;

void setup() {
  /**!
     Initialization of variables.
     1) Global variables (with prefix g_)
     2) Local variables for configuration

     Configuration.
     1) Enable Serial with 115200 baudrate
     2) Configure Pins
     3) Configure Timer Interruption.
     4) Set TCCR0B and TCCR0A to zero. Note: This is an important
        step to reduce the jitter effect of the output signal.
  */

  /* Intializes random number generator */
//  srand((unsigned) time(&t));
  g_isPinStateUpdated = true;
  g_isSerialCompleted = true;
  g_pinNextState = 0;

  float frecuency = 3520;

  Serial.begin(115200);

#if DEBUG==1
  Serial.println("OCC Transmitter");
  Serial.print("Frecuency: ");
  Serial.println(frecuency);
#endif

  configurePins_init();
  configureTimerInterruptionFor(frecuency);

  /*Don't remove*/
  TCCR0B = 0;
  TCCR0A = 0;
  /*End: Don't remove*/

  /**
     External Interrupt
  */
  pinMode(3, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(3), changeState, RISING);
  startTimer();
  occState = OCC_TX;
}

void loop() {

  /**!
     Colaboration architecture
     1) Checks interrupt flag. If the pin state has been flushed, then update next Pin State.
     2) Checks serial. If data is comming, push to converter.
  */
  if (g_isPinStateUpdated) {
    g_pinNextState = converter.getNextState();
    converter.loadNextState();
    g_isPinStateUpdated = false;

  } else {
    switch (occState) {
      /*
         Case OCC_TX: Transmitting Data. Commute between two different states: Idle Signal,
         Data Signal.

         Commands:
         'e': End transmission.
         'd': Commute: (Sending idle or data signals).
      */
      case OCC_TX:
        if (g_sendContiniously) {
          if(converter.insertData(generateRandomData())){
            g_numberOfPacketsSent++;
          };
          if (g_numberOfPacketsSent > MAX_NUMBER_OF_PACKETS) {
            g_numberOfPacketsSent = 0;
            g_sendContiniously = 0;
            Serial.print("f");
          }
        }
        if (Serial.available() > 0) {
          uint8_t command = Serial.read();
          if (command == 'e') {
            stopTimer();
            occState = OCC_STOP;
            converter.reset();
          } else if (command == 'd') {
            g_sendContiniously ^= 1;
          }
        }
        break;

      /*
         Case OCC_STOP:

         Commands:
         's': Start transmission.
         'c': Entering in state of configuration.
      */
      case OCC_STOP:
        if (Serial.available() > 0) {
          uint8_t command = Serial.read();
          if (command == 's') {
            g_sendContiniously = 0;
            g_numberOfPacketsSent = 0;
            startTimer();
            occState = OCC_TX;
          } else if (command == 'c') {
            occState = OCC_CONFIG;
          }
        }
        break;

      /*
        Case OCC_Config:

        Commands:
        'fxxxxx ': Change chip frequency of transmission. (Note the (SPACE) as separator between commands. It is mandatory).
        'x': Exiting the configuration state --> Stop state.
      */
      case OCC_CONFIG:
        if (Serial.available() > 0) {
          uint8_t command = Serial.read();
          if (command == 'f') {
            String frequencyS = Serial.readStringUntil(' ');
            float frequency = float(frequencyS.toInt());
            configureTimerInterruptionFor(frequency);
            g_sendContiniously = 0;
          }
          else if (command == 'x') {
            occState = OCC_STOP;
          }
        }
        break;
    }
  }
}

ISR(TIMER3_COMPA_vect) {
  /**
     1) Update pins.
     2) Update interrupt flag.
  */
  flushNext(g_pinNextState);
  g_isPinStateUpdated = true;
}
